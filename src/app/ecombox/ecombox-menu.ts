import { NbMenuItem } from "@nebular/theme";
import { SERVER_TYPES } from "./shared/api/variables";

export const MENU_ECOMBOX_ITEMS: NbMenuItem[] = [
  {
    title: "Tableau de bord",
    icon: { icon: "home", pack: "fa" },
    link: "/ecombox/dashboard",
    home: true,
    data: {
      permission: "view",
      resource: "dashboard",
    },
  },
  {
    title: "Prestashop",
    icon: { icon: "store", pack: "fa" },
    link: `/ecombox/servers/${SERVER_TYPES.prestashop}`,
    data: {
      permission: "view",
      resource: "presta",
    },
  },
  {
    title: "WooCommerce",
    icon: { icon: "shopping-cart", pack: "fa" },
    link: `/ecombox/servers/${SERVER_TYPES.woocommerce}`,
    data: {
      permission: "view",
      resource: "woocommerce",
    },
  },
  {
    title: "Blog - WordPress",
    icon: { icon: "blog", pack: "fa" },
    link: `/ecombox/servers/${SERVER_TYPES.blog}`,
    data: {
      permission: "view",
      resource: "blog",
    },
  },
  {
    title: "Mautic",
    icon: { icon: "poll", pack: "fa" },
    link: `/ecombox/servers/${SERVER_TYPES.mautic}`,
    data: {
      permission: "view",
      resource: "mautic",
    },
  },
  {
    title: "Suite CRM",
    icon: { icon: "people-arrows", pack: "fa" },
    link: `/ecombox/servers/${SERVER_TYPES.suitecrm}`,
    data: {
      permission: "view",
      resource: "suitecrm",
    },
  },
  {
    title: "Odoo",
    icon: { icon: "project-diagram", pack: "fa" },
    link: `/ecombox/servers/${SERVER_TYPES.odoo}`,
    data: {
      permission: "view",
      resource: "odoo",
    },
  },
  {
    title: "Kanboard",
    icon: { icon: "tasks", pack: "fa" },
    link: `/ecombox/servers/${SERVER_TYPES.kanboard}`,
    data: {
      permission: "view",
      resource: "kanboard",
    },
  },
  {
    title: "HumHub",
    icon: { icon: "users", pack: "fa" },
    link: `/ecombox/servers/${SERVER_TYPES.humhub}`,
    data: {
      permission: "view",
      resource: "humhub",
    },
  },
  {
    title: "Gestion avancée",
    icon: { icon: "exclamation-triangle", pack: "fa" },
    data: {
      permission: "view",
      resource: "admin",
    },
    children: [
      {
        title: "Accès SFTP",
        link: `/ecombox/servers/${SERVER_TYPES.sftp}`,
        data: {
          permission: "view",
          resource: "sftp",
        },
      },
      {
        title: "Accès phpMyAdmin",
        link: `/ecombox/servers/${SERVER_TYPES.pma}`,
        data: {
          permission: "view",
          resource: "pma",
        },
      },
      {
        title: "Accès admin",
        link: "/ecombox/portainer",
        data: {
          permission: "view",
          resource: "portainer",
        },
      },
      {
        title: "Gérer vos modèles de sites",
        link: "/ecombox/images",
        data: {
          permission: "view",
          resource: "images",
        },
      },
    ],
  },
  {
    title: "Aide",
    icon: { icon: "question-circle", pack: "fa" },
    link: "/ecombox/help",
    data: {
      permission: "view",
      resource: "help",
    },
  },
  {
    title: "Mentions légales",
    icon: { icon: "balance-scale", pack: "fa" },
    link: "/ecombox/policies",
    data: {
      permission: "view",
      resource: "policies",
    },
  },
];
