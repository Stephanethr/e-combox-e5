import { Component } from "@angular/core";

@Component({
  selector: "ecx-help",
  templateUrl: "./help.component.html",
  styleUrls: ["./help.component.scss", "./animate.css"],
})
export class HelpComponent {}
