import { NgModule } from "@angular/core";
import { ThemeModule } from "../../@theme/theme.module";
import { TranslateModule } from "@ngx-translate/core";
import { Ng2SmartTableModule } from "ng2-smart-table";
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbProgressBarModule,
  NbListModule,
  NbIconModule,
  NbSpinnerModule,
  NbTreeGridModule,
} from "@nebular/theme";

import { ImagesComponent } from "./images.component";

@NgModule({
  imports: [
    ThemeModule,
    NbActionsModule,
    NbButtonModule,
    NbCardModule,
    NbProgressBarModule,
    NbListModule,
    NbIconModule,
    NbTreeGridModule,
    NbSpinnerModule,
    Ng2SmartTableModule,
    TranslateModule.forChild({
      extend: true,
    }),
  ],
  declarations: [ImagesComponent],
})
export class ImagesModule {}
