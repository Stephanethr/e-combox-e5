import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { NbAccessChecker } from "@nebular/security";
import { ContainerService } from "../shared/api/container.service";

@Component({
  selector: "ecx-policies",
  styleUrls: ["./policies.component.scss"],
  templateUrl: "./policies.component.html",
})
export class PoliciesComponent implements OnInit {
  public notice = {
    owner: "",
    manager: "",
    purpose: "",
    admin: "",
    dpd: "",
    address: "",
    host: "",
    registration: "",
  };

  private path: string = "/opt";
  private todo: string = "<à compléter>";

  /**
   * No Param - void return
   */
  constructor(
    public accessChecker: NbAccessChecker,
    private router: Router,
    private containerService: ContainerService
  ) {
    console.info("e-comBox dev du 03/09/2022");
  }

  /**
   *
   */
  ngOnInit(): void {
    // Get owner
    for (const propt in this.notice) {
      if (Object.prototype.hasOwnProperty.call(this.notice, propt)) {
        this.getData(propt);
      }
    }
  }

  /**
   *
   * @param {string} file
   */
  private getData(file: string): void {
    this.containerService
      .runExecInstance("FSserver", `cat ${this.path}/${file}`, false, "/bin/sh")
      .subscribe((result) => {
        if (result.includes("No such file or directory")) {
          this.notice[file] = this.todo;
        } else {
          this.notice[file] = result.slice(8);
        }
      });
  }

  /**
   *
   */
  onEdit(): void {
    this.router.navigate(["ecombox/edit-policies"]);
  }
}
