import { NgModule } from "@angular/core";
import { ThemeModule } from "../../@theme/theme.module";
import { TranslateModule } from "@ngx-translate/core";
import { PoliciesComponent } from "./policies.component";
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbIconModule,
  NbInputModule,
} from "@nebular/theme";
import { FormPoliciesComponent } from "./form-policies/form-policies.component";
import { FormsModule } from "@angular/forms";

@NgModule({
  imports: [
    NbCardModule,
    ThemeModule,
    NbActionsModule,
    NbButtonModule,
    NbCardModule,
    NbCheckboxModule,
    NbInputModule,
    FormsModule,
    NbIconModule,
    TranslateModule.forChild({
      extend: true,
    }),
  ],
  declarations: [PoliciesComponent, FormPoliciesComponent],
})
export class PolicyModule {}
