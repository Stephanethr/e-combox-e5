import { Component, OnInit } from "@angular/core";
import { NbAccessChecker } from "@nebular/security";
import { NbMenuItem } from "@nebular/theme";

import { MENU_ECOMBOX_ITEMS } from "./ecombox-menu";
import { StackStore } from "./shared/store/stack.store";

@Component({
  selector: "ngx-ecombox",
  templateUrl: "./ecombox.component.html",
  styleUrls: ["./ecombox.component.scss"],
})
export class EcomboxComponent implements OnInit {
  menu = MENU_ECOMBOX_ITEMS;

  /**
   *
   * @param {StackStore} stackStore Class to manage stakcs
   * @param {NbAuthService} authService Authentication service
   */
  constructor(
    private store: StackStore,
    private accessChecker: NbAccessChecker
  ) {}

  /**
   * ngOnInit(): void
   */
  ngOnInit(): void {
    // Check ACL before displaying menu
    this.authMenuItems();
    this.store.loadInitialData();
  }

  /**
   *
   */
  authMenuItems(): void {
    this.menu.forEach((item) => {
      this.authMenuItem(item);
    });
  }

  /**
   *
   * @param {NbMenuItem} menuItem
   */
  authMenuItem(menuItem: NbMenuItem): void {
    if (
      menuItem.data &&
      menuItem.data["permission"] &&
      menuItem.data["resource"]
    ) {
      this.accessChecker
        .isGranted(menuItem.data["permission"], menuItem.data["resource"])
        .subscribe((granted) => {
          menuItem.hidden = !granted;
        });
    } else {
      menuItem.hidden = true;
    }
    if (!menuItem.hidden && menuItem.children != null) {
      menuItem.children.forEach((item) => {
        if (item.data && item.data["permission"] && item.data["resource"]) {
          this.accessChecker
            .isGranted(item.data["permission"], item.data["resource"])
            .subscribe((granted) => {
              item.hidden = !granted;
            });
        } else {
          // if child item do not config any `data.permission` and `data.resource` just inherit parent item's config
          item.hidden = menuItem.hidden;
        }
      });
    }
  }
}
