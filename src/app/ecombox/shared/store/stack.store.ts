/* eslint-disable @typescript-eslint/no-explicit-any */
import { Injectable } from "@angular/core";
import {
  BehaviorSubject,
  forkJoin,
  Observable,
  queueScheduler,
  scheduled,
  throwError,
} from "rxjs";
import { Stack, TypeStack } from "../models/stack";
import { asObservable } from "./asObservable";
import {
  COMPOSE_EXT,
  DOCKER_COMPOSE,
  GET_PARAM_STACK,
  PARAM_STACK_FILE,
  PARAM_STACK_NAME,
  PARAM_SUFFIX_COMPOSE,
  SERVER_TYPES,
  SERVER_WITH_PMA,
  SERVER_WITH_SFTP,
  IMAGES,
} from "../api/variables";
import { StacksService } from "../api/stacks.service";
import { ContainerService } from "../api/container.service";
import { StackEnv } from "../models/stack-env";
import { StackCreateRequest } from "../models/stack-create-request";
import { Container } from "../models/container";
import { concatAll, map, mergeMap, switchMap, tap } from "rxjs/operators";
import { StackFileResponse } from "../models/stack-file-response";
import { StackUpdateRequest } from "../models/stack-update-request";
import { VolumeService } from "../api/volume.service";
import { ToastrService } from "ngx-toastr";
import { Utils } from "../utils";
import { HttpErrorResponse } from "@angular/common/http";

@Injectable()
export class StackStore {
  private _stacks: BehaviorSubject<Stack[]> = new BehaviorSubject<Stack[]>([]);
  public messageSource: BehaviorSubject<string>;
  public stackCreationInProgress: boolean = false;

  private _paramEnv = {
    dockerIP: "",
    nginxPort: "",
    httpProxy: "",
    httpsProxy: "",
    noProxy: "",
    registryPort: "",
    publicURL: "",
    fqdn: "",
    useReverseProxy: "",
    path: "",
  };

  private _names: string[] = [];

  /**
   * Constructor
   * @param {StacksService} stackService Class to manage stakcs
   * @param {ConstainerService} containerService Authentication service
   */
  constructor(
    private stackService: StacksService,
    private containerService: ContainerService,
    private volumeService: VolumeService,
    private toastr: ToastrService
  ) {
    this.toastr.toastrConfig.timeOut = 10000;
    this.messageSource = new BehaviorSubject<string>("");
  }

  /**
   * Unload all the stacks loaded
   */
  unloadData(): void {
    this._stacks = new BehaviorSubject<Stack[]>([]);
  }

  /**
   *
   * @return {Observable<Stack[]>} Observable of list of stacks
   */
  getStacks(): Observable<Stack[]> {
    return asObservable(this._stacks);
  }

  /**
   * @return {Stack[]} Observable of list of stacks
   */
  getPmaStacks(): Stack[] {
    const pmaStacks: Stack[] = [];
    this._stacks.getValue().forEach((stack) => {
      if (stack.getTypeStack() === TypeStack.pma) {
        pmaStacks.push(stack);
      }
    });
    return pmaStacks;
  }

  /**
   * @return {Stack[]} Observable of list of stacks
   */
  getSftpStacks(): Stack[] {
    const sftpStacks: Stack[] = [];
    this._stacks.getValue().forEach((stack) => {
      if (
        stack.getName().startsWith("sftp") ||
        stack.getName().startsWith("pma")
      ) {
        sftpStacks.push(stack);
      }
    });
    return sftpStacks;
  }

  /**
   *
   * @param {number} stackName
   * @return {Stack}
   */
  getStackAssociate(stackName: string): Stack[] {
    const s: Stack[] = [];
    this.getSftpStacks().forEach((stack) => {
      stack.getEnvVars().forEach((envi) => {
        if (envi.value === stackName) {
          s.push(stack);
          return;
        }
      });
    });
    return s;
  }

  /**
   * get nginx reverse proxy port
   * @return {number}
   */
  getNginxPort(): string {
    return this._paramEnv.nginxPort;
  }

  /**
   * get docker IP
   * @return {string}
   */
  getDockerIP(): string {
    return this._paramEnv.dockerIP;
  }

  /**
   * get docker registry port
   * @return {string}
   */
  getResgistryPort(): string {
    return this._paramEnv.registryPort;
  }

  /**
   * get FQDN
   * @return {string}
   */
  getFQDN(): string {
    return this._paramEnv.fqdn;
  }

  /**
   * get if a reverse proxy is uses
   * @return {string}
   */
  getUseReverseProxy(): string {
    return this._paramEnv.useReverseProxy;
  }

  /**
   * get path uses by reverse proxy
   * @return {string}
   */
  getPath(): string {
    return this._paramEnv.path;
  }

  /**
   * get public URL
   * @return {string}
   */
  getPublicURL(): string {
    return this._paramEnv.publicURL;
  }

  /**
   * @return {void}
   */
  unsuscribe(): void {
    this._stacks.unsubscribe();
  }

  /**
   * Load Initial
   */
  loadInitialData(): void {
    forkJoin([
      this.containerService.containerInspect("nginx"),
      this.containerService.containerGet(`{"name":["e-combox_registry"]}`),
      this.containerService.containerGet(`{"name":["e-combox_gitserver"]}`),
    ]).subscribe((result) => {
      result[0].getEnv().forEach((envVar: string) => {
        if (envVar.startsWith("NGINX_HOST")) {
          this._paramEnv.dockerIP = envVar.slice(11);
        } else if (envVar.startsWith("NGINX_PORT")) {
          this._paramEnv.nginxPort = envVar.slice(11);
        } else if (envVar.startsWith("DOMAINE")) {
          this._paramEnv.publicURL = envVar.slice(8);
        } else if (envVar.startsWith("RV")) {
          this._paramEnv.useReverseProxy = envVar.slice(3);
        } else if (envVar.startsWith("FQDN")) {
          this._paramEnv.fqdn = envVar.slice(5);
        } else if (envVar.startsWith("CHEMIN")) {
          this._paramEnv.path = envVar.slice(7);
        } else if (envVar.slice(0, 10) === "HTTP_PROXY") {
          this._paramEnv.httpProxy = envVar.slice(11);
        } else if (envVar.slice(0, 11) === "HTTPS_PROXY") {
          this._paramEnv.httpsProxy = envVar.slice(12);
        } else if (envVar.slice(0, 8) === "NO_PROXY") {
          this._paramEnv.noProxy = envVar.slice(9);
        }
      });

      if (result[2][0]) {
        GET_PARAM_STACK.repositoryURL = `http://${result[2][0].getIpAddress()}/git/e-comBox_docker-compose`;
      }
      if (result[1][0]) {
        this._paramEnv.registryPort = result[1][0].getPort().toString();
      }
      this.messageSource.next("update");
    });

    this.stackService.stackList().subscribe((stacks) => {
      this._stacks.next(stacks);
      this._stacks.getValue().forEach((element: Stack) => {
        // get informations of a container only if the stack is started
        if (
          (element.getContainers() === undefined ||
            element.getContainers().length === 0) &&
          element.getStatus() === 1
        ) {
          this.addContainerToStack(element.getName(), false, true);
        }
      });
    });
  }

  /**
   * Stack creation
   * @param {string} serverType Class to manage stakcs
   * @param {string} db DB type
   * @param {string} suffix? Mandatory for creation stack
   * @param {boolean} custom
   * @param {string} tag
   * @param {number} nb
   */
  createStackSequentiallyStacks(
    serverType: string,
    db: string,
    suffix?: string,
    custom: boolean = false,
    tag: string = "",
    nb: number = 1
  ): void {
    this.stackCreationInProgress = true;
    const requests = [];
    if (nb === 1) {
      requests.push(this.createStack(serverType, db, suffix, custom, tag));
    } else {
      for (let i = 1; i <= nb; i++) {
        requests.push(
          this.createStack(serverType, db, suffix + i, custom, tag)
        );
      }
    }

    // To create stacks one by one
    scheduled(requests, queueScheduler)
      .pipe(concatAll())
      .subscribe(
        () => {},
        (error: HttpErrorResponse) => {
          this.stackCreationInProgress = false;
          // Error encountered only on Debian 9 and Debian 10 VM
          // The stack is operational but the request is interrupted before the end
          if (error.status === 0) {
            this.loadInitialData();
          }
        },
        () => {
          this.stackCreationInProgress = false;
        }
      );
    // }
  }

  /**
   * Stack creation
   * @param {string} serverType Class to manage stakcs
   * @param {string|Stack} dbOrStack Main stack for SFTP or PMA
   * @param {string} suffix? Mandatory for creation stack
   * @param {boolean} custom
   * @param {string} tag
   * @return {Observable<Stack>}
   */
  createStack(
    serverType: string,
    dbOrStack: string | Stack,
    suffix?: string,
    custom: boolean = false,
    tag: string = ""
  ): Observable<Container[]> {
    let body: StackCreateRequest;

    if (dbOrStack instanceof Stack) {
      body = this.prepareCreateLinkedStackRequest(serverType, dbOrStack);
    } else {
      body = this.prepareCreateStackRequest(
        serverType,
        dbOrStack,
        suffix,
        custom,
        tag
      );
    }

    if (body === undefined) {
      return throwError("Error during prepare request create stack");
    }

    return this.stackService
      .stackCreate(
        GET_PARAM_STACK.type,
        GET_PARAM_STACK.method,
        GET_PARAM_STACK.endpointID,
        body
      )
      .pipe(
        mergeMap((stack: Stack) => {
          const index = this._names.indexOf(stack.getName());
          if (index !== -1) {
            this._names.splice(index, 1);
          }
          this._stacks.getValue().push(stack);
          return this.addContainerToStack(stack.getName(), custom);
        })
      );
  }

  /**
   *
   * @param {string} serverType
   * @param {string} dbType
   * @param {string} suffix
   * @param {boolean} custom
   * @param {string} tag
   * @return {StackCreateRequest}
   */
  prepareCreateStackRequest(
    serverType: string,
    dbType: string,
    suffix: string,
    custom: boolean,
    tag
  ): StackCreateRequest {
    const stack: Stack = new Stack();

    let envSuffix;
    let ext = PARAM_STACK_FILE[dbType];
    ext += COMPOSE_EXT;

    if (custom) {
      envSuffix = tag + "-" + suffix;
      stack.setName(serverType + tag + suffix);
    } else {
      envSuffix = PARAM_SUFFIX_COMPOSE[dbType] + suffix;
      stack.setName(serverType + PARAM_STACK_NAME[dbType] + suffix);
    }

    const index = this._names.indexOf(stack.getName());
    if (index !== -1) {
      this.toastr.warning(
        `Un site ${serverType}-${envSuffix} est déjà en cours de création. Veuillez choisir un autre nom`
      );
      return undefined;
    } else {
      this._names.push(stack.getName());
    }

    let composeFile = DOCKER_COMPOSE + serverType;
    if (custom) {
      composeFile += "-custom";
    }
    composeFile += ext;

    const passwdDB: string = Utils.getRandomString(20);
    const passwdRoot: string = Utils.getRandomString(20);

    if (!custom) {
      tag = "";
    }

    let dockerHost: string = this.getDockerIP();
    if (this._paramEnv.useReverseProxy === "N") {
      if (this.getPublicURL() !== undefined && this.getPublicURL() !== "") {
        dockerHost = this.getPublicURL();
      }
    }

    stack.initEnvVariables(
      envSuffix,
      this._paramEnv.httpProxy,
      this._paramEnv.httpsProxy,
      this._paramEnv.noProxy,
      passwdDB,
      passwdRoot,
      dockerHost,
      this.getNginxPort(),
      this.getResgistryPort(),
      this.getPublicURL(),
      this.getFQDN(),
      this.getUseReverseProxy(),
      this.getPath(),
      tag
    );

    const body: StackCreateRequest = this.initCreateBodyRequest(
      stack.getName(),
      composeFile
    );
    body.env = stack.getEnvVars();
    return body;
  }

  /**
   * @param {string} linkedType
   * @param {Stack} mainStack
   * @return {StackCreateRequest}
   */
  prepareCreateLinkedStackRequest(
    linkedType: string,
    mainStack: Stack
  ): StackCreateRequest {
    const stack: Stack = new Stack();
    let composeFile: string;

    if (linkedType === SERVER_TYPES.sftp) {
      stack.setName(SERVER_TYPES.sftp + mainStack.getName());
      composeFile =
        DOCKER_COMPOSE +
        SERVER_TYPES.sftp +
        "-" +
        mainStack.getServerType() +
        COMPOSE_EXT;

      let path: string = "";
      if (this.getUseReverseProxy() === "O") {
        path = `/${this.getPath()}`;
      }
      stack.initSFTPEnvVariables(
        mainStack.getName(),
        Utils.getRandomString(6),
        path
      );
      // PMA
    } else if (linkedType === SERVER_TYPES.pma) {
      stack.setName(SERVER_TYPES.pma + mainStack.getName());
      if (
        mainStack.getServerType() === SERVER_TYPES.blog ||
        mainStack.getServerType() === SERVER_TYPES.woocommerce
      ) {
        composeFile =
          DOCKER_COMPOSE + SERVER_TYPES.pma + "-wordpress" + COMPOSE_EXT;
      } else {
        composeFile =
          DOCKER_COMPOSE +
          SERVER_TYPES.pma +
          "-" +
          mainStack.getServerType() +
          COMPOSE_EXT;
      }
      const suffix: string = mainStack
        .getEnvVars()
        .find((e) => e.name === "SUFFIXE").value;

      const passwdDB: string = mainStack
        .getEnvVars()
        .find((e) => e.name === "DB_PASS").value;

      stack.initPMAEnvVariables(
        mainStack.getName(),
        passwdDB,
        mainStack.getServerType(),
        suffix
      );
    }

    const body: StackCreateRequest = this.initCreateBodyRequest(
      stack.getName(),
      composeFile
    );
    body.env = stack.getEnvVars();
    return body;
  }
  /**
   * Delete a stack and associated volumes
   * @param {number} id stack's id
   * @param {string} name stack's mane
   * @return {Observable<any>}
   */
  deleteStack(id: number, name: string): Observable<any> {
    const obs = this.stackService.stackDelete(id).pipe(
      tap(() => {
        let message = "";
        if (name.startsWith("pma")) {
          message = "Le serveur PMA correspondant a été arrêté";
        } else if (name.startsWith("sftp")) {
          message = "Le serveur SFTP correspondant a été arrêté";
        } else {
          message = "Le serveur " + name + " a été supprimé";
        }
        this.toastr.info(message);

        this._stacks.getValue().splice(this.getIndexFromName(name), 1);
        const buffer: Stack[] = this._stacks.getValue();
        this._stacks.next(buffer);
      }),
      switchMap(() =>
        this.volumeService.volumesList(`{"name":["${name}"]}`).pipe(
          mergeMap((data) =>
            data.Volumes.map((n) => {
              this.volumeService.volumeRemove(n.Name).subscribe();
            })
          )
        )
      )
    );
    return obs;
  }

  /**
   * @param {number} id
   * @return {Observable<void>}
   */
  public startStack(id: number): Observable<void> {
    return this.stackService.stackStart(id).pipe(
      map((data) => {
        const index = this.getIndexFromName(data.getName());
        this._stacks.getValue()[index].setStatus(data.getStatus());
      })
    );
  }

  /**
   * @param {number} id
   * @return {Observable<void>}
   */
  public stopStack(id: number): Observable<void> {
    return this.stackService.stackStop(id).pipe(
      map((data: Stack) => {
        this.toastr.info(`Le site ${data.getName()} a été arrêté !`);
        this.deleteLinkedStack(data);

        const index = this.getIndexFromName(data.getName());
        this._stacks.getValue()[index].setStatus(data.getStatus());
        this._stacks.getValue()[index].setContainers(undefined);
        const buffer: Stack[] = this._stacks.getValue();
        this._stacks.next(buffer);
      })
    );
  }

  /**
   *
   * @param {Stack} stack
   */
  public deleteLinkedStack(stack: Stack): void {
    if (
      SERVER_WITH_PMA[stack.getServerType()] ||
      SERVER_WITH_SFTP[stack.getServerType()]
    ) {
      const s = this.getStackAssociate(stack.getName());
      if (s.length > 0) {
        s.forEach((sSFTPorPMA) => {
          this.deleteStack(
            sSFTPorPMA.getID(),
            sSFTPorPMA.getName()
          ).subscribe();
        });
      }
    }
  }

  /**
   * @param {number} id stack's id
   * @param {number} type server type
   * @param {boolean} customv3 to check if it's an old version
   * @return {Observable<Container[]>}
   */
  public updateStack(
    id: number,
    type: string,
    customv3: boolean = false
  ): Observable<unknown> {
    const s = this.getStack(id);

    const vars: StackEnv[] = s.getEnvVars();
    const newVars: StackEnv[] = [];

    // Specific for migration from v2 to v3
    if (vars.find((e) => e.name === "HTTP_PROXY") === undefined) {
      vars.push({ name: "HTTP_PROXY", value: "" });
      vars.push({ name: "http_proxy", value: "" });
      vars.push({ name: "HTTPS_PROXY", value: "" });
      vars.push({ name: "https_proxy", value: "" });
      vars.push({ name: "NO_PROXY", value: "" });
      vars.push({ name: "no_proxy", value: "" });
    }

    // Specific for migration from v2 to v3
    if (vars.find((e) => e.name === "ECB_DOCKER_HOST") === undefined) {
      vars.push({ name: "ECB_DOCKER_HOST", value: "" });
      vars.push({ name: "NGINX_PORT", value: "" });
    }

    // Specific for migration from v2 to v3
    if (vars.find((e) => e.name === "ROOT_DB_PASS") === undefined) {
      const passwdRoot: string = Utils.getRandomString(20);
      vars.push({ name: "ROOT_DB_PASS", value: passwdRoot });
    }

    // // Specific for migration from v3 to v4
    if (vars.find((e) => e.name === "DOMAINE") === undefined) {
      vars.push({ name: "DOMAINE", value: "" });
      vars.push({ name: "RV", value: "" });
      vars.push({ name: "FQDN", value: "" });
      vars.push({ name: "CHEMIN", value: "" });
    }

    // Update for changing network environment
    vars.forEach((envVar) => {
      if (envVar.name === "HTTP_PROXY" || envVar.name === "http_proxy") {
        envVar.value = this._paramEnv.httpProxy;
      }
      if (envVar.name === "HTTPS_PROXY" || envVar.name === "https_proxy") {
        envVar.value = this._paramEnv.httpsProxy;
      }
      if (envVar.name === "NO_PROXY" || envVar.name === "no_proxy") {
        envVar.value = this._paramEnv.noProxy;
      }
      if (envVar.name === "ECB_DOCKER_HOST") {
        if (this.getUseReverseProxy() === "N") {
          if (this.getPublicURL() !== "" && this.getPublicURL() !== undefined) {
            envVar.value = this.getPublicURL();
          } else {
            envVar.value = this.getDockerIP();
          }
        } else {
          envVar.value = this.getDockerIP();
        }
      }
      if (envVar.name === "NGINX_PORT") {
        envVar.value = this.getNginxPort();
      }
      if (envVar.name === "DOMAINE") {
        envVar.value = this.getPublicURL();
      }
      if (envVar.name === "RV") {
        envVar.value = this.getUseReverseProxy();
      }
      if (envVar.name === "FQDN") {
        envVar.value = this.getFQDN();
      }
      if (envVar.name === "CHEMIN") {
        envVar.value = this.getPath();
      }
      newVars.push(envVar);
    });

    const body: StackUpdateRequest = {
      env: newVars,
      prune: false,
    };

    return this.stackService.stackFileInspect(id).pipe(
      switchMap((composeFile) => {
        if (customv3) {
          console.debug("Type : " + IMAGES[type]);
          composeFile.StackFileContent = composeFile.StackFileContent.replace(
            "localhost:${REGISTRY_PORT}/" + type + ":${TAG}",
            IMAGES[type]
          );
          return this.stackService.stackUpdate(id, {
            stackFileContent: composeFile.StackFileContent,
            ...body,
          });
        } else {
          // Refactor - For portainer version 2.5.0, needs to start a stack before update it
          return this.startStack(id).pipe(
            switchMap(() => {
              return this.stackService.stackUpdate(id, {
                stackFileContent: composeFile.StackFileContent,
                ...body,
              });
            }),
            switchMap((s: Stack) => this.addContainerToStack(s.getName()))
          );
        }
      })
    );
  }

  /**
   *
   * @param {string} stackName
   * @param {boolean} custom
   * @param {boolean} initialLoading
   * @return {Observable<Container[]>}
   */
  private addContainerToStack(
    stackName: string,
    custom: boolean = false,
    initialLoading: boolean = false
  ): Observable<Container[]> {
    const i = this.getIndexFromName(stackName);
    let obs: Observable<Container[]>;

    if (this._stacks.getValue()[i].getContainers() === undefined) {
      obs = this.containerService.containerGet(
        `{"label":["com.docker.compose.project=${stackName}"]}`
      );
      obs.subscribe(
        (res) => {
          res.forEach((container, index) => {
            if (
              !container.getName().startsWith("sftp") &&
              !container.getName().startsWith("pma")
            ) {
              // Stop stack if a container is stopped
              // @TODO - Refactor
              if (container.getState() === "exited") {
                this.stopStack(this._stacks.getValue()[i].getID()).subscribe();
              } else {
                let domain: string = "";
                let port: string = null;
                if (this.getUseReverseProxy() === "O") {
                  domain = this.getFQDN();
                } else {
                  if (
                    this.getPublicURL() !== "" &&
                    this.getPublicURL() !== undefined
                  ) {
                    domain = this.getPublicURL();
                  } else {
                    domain = this.getDockerIP();
                  }

                  port = this.getNginxPort();
                }
                const url: string = container.buildBackOfficeURL(
                  domain,
                  port,
                  this._stacks.getValue()[i].getServerType()
                );
                res[index].setUrlBackOffice(url);
              }
            }
          });

          if (!initialLoading) {
            // 2 seconds of delay for nginx
            setTimeout(() => {
              this._stacks.getValue()[i].setContainers(res);
              const buffer: Stack[] = this._stacks.getValue();
              // TESTS
              if (
                custom &&
                !this._stacks.getValue()[i].getServerType().includes("odoo") &&
                !this._stacks.getValue()[i].getServerType().includes("suitecrm")
              ) {
                // if custom image we have to check the version with a docker exec command
                this.containerService
                  .runExecInstance(
                    res[0].getName(),
                    "cat /tmp/config-site.sh | grep URL | grep https"
                  )
                  .pipe(
                    switchMap((data) => {
                      console.debug("Résultat de la commande exec : " + data);
                      if (data === "") {
                        return this.updateStack(
                          this._stacks.getValue()[i].getID(),
                          this._stacks.getValue()[i].getServerType(),
                          custom
                        ).pipe(
                          map(() => {
                            this._stacks.next(buffer);
                            this.toastr.info(
                              `Votre site ${res[0].getName()} est prêt !`
                            );
                          })
                        );
                      } else {
                        this._stacks.next(buffer);
                        this.toastr.info(
                          `Votre site ${res[0].getName()} est prêt !`
                        );
                        return null;
                      }
                    })
                  )
                  .subscribe();
              } else {
                this._stacks.next(buffer);
                this.toastr.info(`Votre site ${res[0].getName()} est prêt !`);
              }
            }, 2000);
          } else {
            this._stacks.getValue()[i].setContainers(res);
            const buffer: Stack[] = this._stacks.getValue();
            this._stacks.next(buffer);
          }
        },
        (error) => {
          if (!initialLoading) {
            this.toastr.error(`Un problème est survenu : ${error.message}`);
          }
        }
      );
    }
    return obs;
  }

  /**
   * Find a stack with its ID
   * @param {number} id
   * @return {Stack}
   */
  public getStack(id: number): Stack {
    return this._stacks.getValue().find((e) => e.getID() === id);
  }

  /**
   *
   * @param {string} stackName
   * @return {number}
   */
  public getIndexFromName(stackName: string): number {
    return this._stacks.getValue().findIndex((e) => e.getName() === stackName);
  }

  /**
   *
   * @param {number} id stack's id
   * @return {Container} returns db container
   */
  public getStackFileContent(id: number): Observable<StackFileResponse> {
    return this.stackService.stackFileInspect(id);
  }

  /**
   *
   * @param {string} name
   * @param {string} composeFile
   * @return {StackCreateRequest}
   */
  private initCreateBodyRequest(
    name: string,
    composeFile: string
  ): StackCreateRequest {
    const param: StackCreateRequest = {
      name: name,
      composeFile: composeFile,
      repositoryURL: GET_PARAM_STACK.repositoryURL,
      repositoryReferenceName: GET_PARAM_STACK.repositoryReferenceName,
      repositoryAuthentication: false,
    };

    return param;
  }
}
