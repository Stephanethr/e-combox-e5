export interface StackMigratePayload {
  /**
   * Endpoint identifier of the target endpoint where the stack will be relocated
   */
  endpointID: number;
  /**
   * If provided will rename the migrated stack
   */
  name?: string;
  /**
   * Swarm cluster identifier, must match the identifier of the cluster where the stack will be relocated
   */
  swarmID?: string;
}
