import { StackEnv } from "./stack-env";
import { Container } from "./container";
import { Injectable } from "@angular/core";
import { Adapter } from "./adapter";
import { SERVER_TYPES } from "../api/variables";

export enum TypeStack {
  default = 1,
  sftp = 2,
  pma = 3,
}

/**
 * Stack Model
 */
export class Stack {
  private id?: number;
  private name?: string;
  private status?: number;
  private envVars?: Array<StackEnv>;
  private repoURL?: string;
  private repoRef?: string;
  private repoAuth?: boolean;
  private repoCommand?: string;
  private fileName?: string;
  private version?: string;
  private type?: TypeStack;
  private mainStack?: Stack;
  private containers: Array<Container>;
  private serverType: string;

  /**
   * Stack constructor
   * @param {number} id stack ID.
   * @param {string} name stack name.
   * @param {number} status stacks status - 1: active, 2: inactive
   * @param {string} repoURL repository URL.
   * @param {Array} env environments variables.
   */
  constructor(
    id?: number,
    name?: string,
    status?: number,
    repoURL?: string,
    env?: Array<StackEnv>
  ) {
    if (id !== undefined) {
      this.id = id;
      this.name = name;
      this.status = status;
      this.repoURL = repoURL;
      this.envVars = env;
      this.containers = [];
      this.serverType = this.extractServerType();
      this.setTypeStack();
    }
  }

  /**
   * @return {Array} return containers of the stack
   */
  public getContainers(): Container[] {
    if (this.containers && this.containers.length > 0) {
      return this.containers;
    }
    return undefined;
  }

  /**
   * @param {TypeStack} type
   */
  public setTypeStack(): void {
    if (this.name.startsWith("sftp")) {
      this.type = TypeStack.sftp;
    } else if (this.name.startsWith("pma")) {
      this.type = TypeStack.pma;
    } else {
      this.type = TypeStack.default;
    }
  }

  /**
   * @return {TypeStack} return containers of the stack
   */
  public getTypeStack(): TypeStack {
    return this.type;
  }

  /**
   * @param {Array} containers set containers stack
   */
  public setContainers(containers: Container[]): void {
    this.containers = containers;
  }

  /**
   * @return {string} return containers of the stack
   */
  public getName(): string {
    return this.name;
  }

  /**
   * @param {string} name
   */
  public setName(name: string): void {
    this.name = name;
  }

  /**
   * @return {number} return status stack - 1: active, 2: inactive
   */
  public getStatus(): number {
    return this.status;
  }

  /**
   * @param {number} status set status stack
   */
  public setStatus(status: number): void {
    this.status = status;
  }

  /**
   * @return {number} return containers of the stack
   */
  public getID(): number {
    return this.id;
  }

  /**
   * @return {string} return containers of the stack
   */
  public getServerType(): string {
    return this.serverType;
  }

  /**
   * @return {StackEnv[]} return containers of the stack
   */
  public getEnvVars(): StackEnv[] {
    return this.envVars;
  }

  /**
   * Get the DB container of the stack
   * @return {Container} returns db container
   */
  public getDbContainer(): Container {
    if (this.containers !== undefined && this.containers !== null) {
      return this.containers.find((e) => e.getApp() === "ecombox-db");
    }
    return undefined;
  }

  /**
   * Get the main container of the stack
   * @return {Container} return website container
   */
  public getMainContainer(): Container {
    if (this.containers !== undefined && this.containers !== null) {
      return this.containers.find((e) => e.getApp() === "ecombox");
    }
    return undefined;
  }

  /**
   * TODO : find better method
   * @return {string}
   */
  private extractServerType(): string {
    let type: string = "";
    Object.keys(SERVER_TYPES).forEach((value: string) => {
      // SERVER_TYPES.forEach((servType: string, ) => {
      if (this.name.startsWith(value)) {
        type = value;
        return;
      }
    });
    return type;
  }

  /**
   *
   * @param {string} name
   * @return {string}
   */
  public getSuffixFromName(): string {
    let suffix = this.name.substring(this.serverType.length - 1);
    if (
      this.name.includes("art") ||
      this.name.includes("v12") ||
      this.name.includes("v13")
    ) {
      suffix = suffix.substring(2);
    }
    return suffix;
  }

  /**
   *
   * @param {string} suffix website name
   * @param {string} httpProxy
   * @param {string} httpsProxy
   * @param {string} noProxy
   * @param {string} passwd
   * @param {string} rootPasswd
   * @param {string} dockerHost
   * @param {string} nginxPort
   * @param {string} registryPort
   * @param {string} publicURL
   * @param {string} fqdn
   * @param {string} useReverseProxy
   * @param {string} path
   * @param {string} tag If image custom
   */
  public initEnvVariables(
    suffix: string,
    httpProxy: string,
    httpsProxy: string,
    noProxy,
    passwd: string,
    rootPasswd: string,
    dockerHost: string,
    nginxPort: string,
    registryPort: string,
    publicURL: string,
    fqdn: string,
    useReverseProxy: string,
    path: string,
    tag: string = ""
  ): void {
    this.envVars = [
      {
        name: "SUFFIXE",
        value: suffix,
      },
      {
        name: "HTTP_PROXY",
        value: httpProxy || "",
      },
      {
        name: "HTTPS_PROXY",
        value: httpsProxy || "",
      },
      {
        name: "http_proxy",
        value: httpProxy || "",
      },
      {
        name: "https_proxy",
        value: httpsProxy || "",
      },
      {
        name: "NO_PROXY",
        value: noProxy || "",
      },
      {
        name: "no_proxy",
        value: noProxy || "",
      },
      {
        name: "DB_PASS",
        value: passwd || "",
      },
      {
        name: "ROOT_DB_PASS",
        value: rootPasswd || "",
      },
      {
        name: "ECB_DOCKER_HOST",
        value: dockerHost || "",
      },
      {
        name: "NGINX_PORT",
        value: nginxPort || "",
      },
      {
        name: "REGISTRY_PORT",
        value: registryPort,
      },
      {
        name: "DOMAINE",
        value: publicURL,
      },
      {
        name: "FQDN",
        value: fqdn,
      },
      {
        name: "RV",
        value: useReverseProxy,
      },
      {
        name: "CHEMIN",
        value: path,
      },
      {
        name: "TAG",
        value: tag,
      },
      {
        name: "COMPOSE_HTTP_TIMEOUT",
        value: "1000",
      },
    ];
  }

  /**
   *
   * @param {string} nameMainSTack
   * @param {string} passwd
   * @param {string} path Used only there is a reverse-proxy
   */
  public initSFTPEnvVariables(
    nameMainSTack: string,
    passwd: string,
    path: string
  ): void {
    this.envVars = [
      {
        name: "NOMSTACK",
        value: nameMainSTack,
      },
      {
        name: "SFTP_USER_PASS",
        value: passwd,
      },
      {
        name: "SFTP_ADMIN_PASS",
        value: passwd,
      },
      {
        name: "CHEMIN_ABSOLU",
        value: path,
      },
      {
        name: "VIRTUAL_HOST",
        value: SERVER_TYPES.sftp + "-" + nameMainSTack,
      },
    ];
  }

  /**
   *
   * @param {string} nameMainSTack
   * @param {string} passwd
   * @param {string} server
   * @param {string} suffix
   */
  public initPMAEnvVariables(
    nameMainSTack: string,
    passwd: string,
    server: string,
    suffix: string
  ): void {
    this.envVars = [
      {
        name: "NOMSTACK",
        value: nameMainSTack,
      },
      {
        name: "DB_PASS",
        value: passwd,
      },
      {
        name: "NOMCONTENEUR_DB",
        value: `${server}-db-${suffix}`,
      },
    ];
  }
}

@Injectable({
  providedIn: "root",
})
export class StackAdapter implements Adapter<Stack> {
  /**
   * @param {any} item stack item from portainer API
   * @return {Stack} return a Stack object
   */
  adapt(item): Stack {
    return new Stack(
      item.Id,
      item.Name,
      item.Status,
      item.EntryPoint,
      item.Env
    );
  }
}
