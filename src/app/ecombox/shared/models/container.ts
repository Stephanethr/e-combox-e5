/* eslint-disable @typescript-eslint/no-explicit-any */
import { Injectable } from "@angular/core";
import { BACK_OFFICE } from "../api/variables";
import { Adapter } from "./adapter";

export class Container {
  private id?: string;
  private name?: string;
  private image?: string;
  private state?: string;
  private urlBackOffice?: string;
  private env?: Array<string>;
  private app: string;
  private sizeRw?: number;
  private sizeRootFs?: number;
  private port?: number;
  private ipAddress?: string;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private labels: any;

  /**
   * Constructor
   * @param {string} id
   * @param {string} name
   * @param {string} image
   * @param {string} state
   * @param {number} port
   * @param {string} ip
   * @param {any} labels
   * @param {string} app Optionnal
   * @param {Array<StackEnv>} env Optionnal
   */
  constructor(
    id: string,
    name: string,
    image: string,
    state: string,
    port: number,
    ip: string,
    labels: any,
    app?: string,
    env?: Array<string>
  ) {
    this.id = id;
    this.name = name;
    this.image = image;
    this.state = state;
    this.port = port;
    this.ipAddress = ip;
    this.labels = labels;
    this.app = app;
    if (env) {
      this.env = env;
    }
  }

  /**
   * Get ID of the container
   * @return {string}
   */
  public getID(): string {
    return this.id;
  }

  /**
   * Get name of the container
   * @return {string}
   */
  public getName(): string {
    return this.name;
  }

  /**
   * Get image name used by the container
   * @return {string}
   */
  public getImage(): string {
    return this.image;
  }

  /**
   * Get sizeRw  used by the container
   * @return {number}
   */
  public getSizeRw(): number {
    return this.sizeRw;
  }

  /**
   * Get sizeRootFs  used by the container
   * @return {number}
   */
  public getSizeRootFs(): number {
    return this.sizeRootFs;
  }

  /**
   * Get sizeRootFs  used by the container
   * @return {number}
   */
  public getPort(): number {
    return this.port;
  }

  /**
   * Get ip's container
   * @return {string}
   */
  public getIpAddress(): string {
    return this.ipAddress;
  }

  /**
   * Get state's container
   * exited if container is stopped
   * running if container is started
   * @return {string}
   */
  public getState(): string {
    return this.state;
  }

  /**
   * Get app's container
   * @return {string}
   */
  public getApp(): string {
    return this.app;
  }

  /**
   * Get url's container
   * @return {string}
   */
  public getUrlBackOffice(): string {
    return this.urlBackOffice;
  }

  /**
   * Set state's container
   * @param {string} state
   * @return {void}
   */
  public setState(state: string): void {
    this.state = state;
  }

  /**
   * Set url's container
   * @param {string} url
   * @return {void}
   */
  public setUrlBackOffice(url: string): void {
    this.urlBackOffice = url;
  }

  /**
   * Get CPU usage of the container
   * @return {number}
   */
  public getUsageCPU(): number {
    return 0;
  }

  /**
   * Get memory usage of the container
   * @return {number}
   */
  public getUsageMem(): number {
    return 0;
  }

  /**
   * Get disk usage of the container
   * @return {number}
   */
  public getUsageDisk(): number {
    return 0;
  }
  /**
   * Get container environment
   * @return {Array<string>}
   */
  public getEnv(): Array<string> {
    return this.env;
  }

  /**
   *
   * @param {string} host
   * @param {string} port
   * @param {string} type
   * @return {string}
   */
  public buildBackOfficeURL(host: string, port: string, type: string): string {
    let url: string = "";
    if (port !== null) {
      url = `https://${host}:${port}/${this.name}${BACK_OFFICE[type]}/`;
    } else {
      url = `https://${host}/${this.name}${BACK_OFFICE[type]}/`;
    }
    return url;
  }
}

@Injectable({
  providedIn: "root",
})
export class ContainerAdapter implements Adapter<Container> {
  /**
   * Create new container from json object
   * @param {any} item
   * @return {Container}
   */
  adapt(item): Container {
    let name: string = "";
    let app: string = "";
    if (item.Names) {
      name = item.Names[0];
    } else {
      name = item.Name;
    }

    if (item.Labels) {
      app = item.Labels["com.docker.compose.app"];
    }

    let port;
    if (item.Ports === undefined || item.Ports.lentgh === 0) {
      port = undefined;
    } else {
      if (item.Ports.length === 0) {
        port = undefined;
      } else {
        port = item.Ports[0].PublicPort;
      }
    }
    return new Container(
      item.Id,
      name.substring(1),
      item.Image,
      item.State,
      port,
      item.NetworkSettings
        ? item.NetworkSettings.Networks["bridge_e-combox"].IPAddress
        : "",
      item.Labels,
      app,
      item.Config?.Env
    );
  }
}
