/* eslint-disable @typescript-eslint/no-explicit-any */
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { forkJoin, Observable } from "rxjs";
import { switchMap, timeout } from "rxjs/operators";

@Injectable()
export class DockerRegistryService {
  private endpoint = "registry/v2";

  /**
   *
   * @param {HttpClient} httpClient
   */
  constructor(protected httpClient: HttpClient) {}

  /**
   * @param {string} domain
   * @param {string} nginxPort
   * @return {Observable<any>}
   */
  public registryImages(domain: string, nginxPort: string): Observable<any> {
    if (nginxPort !== null) {
      return this.httpClient
        .get<any>(`https://${domain}:${nginxPort}/${this.endpoint}/_catalog`, {
          reportProgress: false,
        })
        .pipe(timeout(3000));
    } else {
      return this.httpClient
        .get<any>(`https://${domain}/${this.endpoint}/_catalog`, {
          reportProgress: false,
        })
        .pipe(timeout(3000));
    }
  }

  /**
   * @param {string} image
   * @param {string} domain
   * @param {string} nginxPort
   * @return {Observable<any>}
   */
  public registryTags(
    image: string,
    domain: string,
    nginxPort: string
  ): Observable<any> {
    if (nginxPort !== null) {
      return this.httpClient
        .get<any>(
          `https://${domain}:${nginxPort}/${this.endpoint}/${image}/tags/list`,
          {
            reportProgress: false,
          }
        )
        .pipe(timeout(3000));
    } else {
      return this.httpClient
        .get<any>(`https://${domain}/${this.endpoint}/${image}/tags/list`, {
          reportProgress: false,
        })
        .pipe(timeout(3000));
    }
  }

  /**
   * @param {string} image
   * @param {string} tag
   * @param {string} domain
   * @param {string} nginxPort
   * @return {Observable<any>}
   */
  public registryManifest(
    image: string,
    tag: string,
    domain: string,
    nginxPort: string
  ): Observable<any> {
    if (nginxPort !== null) {
      return this.httpClient.get<any>(
        `https://${domain}:${nginxPort}/${this.endpoint}/${image}/manifests/${tag}`,
        {
          headers: {
            Accept: ["application/vnd.docker.distribution.manifest.v2+json"],
          },
          reportProgress: false,
          observe: "response",
        }
      );
    } else {
      return this.httpClient.get<any>(
        `https://${domain}/${this.endpoint}/${image}/manifests/${tag}`,
        {
          headers: {
            Accept: ["application/vnd.docker.distribution.manifest.v2+json"],
          },
          reportProgress: false,
          observe: "response",
        }
      );
    }
  }

  /**
   *
   * @param {string} domain
   * @param {string} nginxPort
   * @return {Observable<any>}
   */
  getAllTags(domain: string, nginxPort: string): Observable<any> {
    return this.registryImages(domain, nginxPort).pipe(
      switchMap((repo: any) => {
        const requestsTags = [];
        repo.repositories.forEach((name: string) => {
          if (!name.includes("-db")) {
            requestsTags.push(this.registryTags(name, domain, nginxPort));
          }
        });
        return forkJoin(requestsTags);
      })
    );
  }
  /**
   * @param {string} image
   * @param {string} tag
   * @param {string} domain
   * @param {string} nginxPort
   * @return {Observable<any>}
   */
  public registryDelImage(
    image: string,
    tag: string,
    domain: string,
    nginxPort: string
  ): Observable<any> {
    return this.registryManifest(image, tag, domain, nginxPort).pipe(
      switchMap((res) => {
        const digest = res.headers.get("Docker-Content-Digest");

        if (digest != null) {
          if (nginxPort !== null) {
            return this.httpClient.delete<any>(
              `https://${domain}:${nginxPort}/${this.endpoint}/${image}/manifests/${digest}`,
              {
                headers: {
                  Accept: [
                    "application/vnd.docker.distribution.manifest.v2+json",
                  ],
                },
                reportProgress: false,
                observe: "body",
              }
            );
          } else {
            return this.httpClient.delete<any>(
              `https://${domain}/${this.endpoint}/${image}/manifests/${digest}`,
              {
                headers: {
                  Accept: [
                    "application/vnd.docker.distribution.manifest.v2+json",
                  ],
                },
                reportProgress: false,
                observe: "body",
              }
            );
          }
        } else {
          return null;
        }
      })
    );
  }
}
