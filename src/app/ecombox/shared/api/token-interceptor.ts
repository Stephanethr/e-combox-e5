/* eslint-disable @typescript-eslint/no-explicit-any */
import { Injectable } from "@angular/core";
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpResponse,
  HttpErrorResponse,
} from "@angular/common/http";
import { BehaviorSubject, Observable, throwError } from "rxjs";
import { AuthService } from "./auth.service";
import { map, catchError } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";
// import { AuthenticateUserResponse } from "../models/authenticate-user-response";
import { NbAuthJWTToken, NbAuthService } from "@nebular/auth";
import { Router } from "@angular/router";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  private refreshTokenInProgress: boolean = false;
  private refreshTokenSubject: BehaviorSubject<unknown> = new BehaviorSubject<
    unknown
  >(null);

  private token: string;

  /**
   * Constructor
   * @param {AuthService} auth
   */
  constructor(
    public auth: AuthService,
    private toastr: ToastrService,
    private authService: NbAuthService,
    private router: Router
  ) {
    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      if (token.isValid()) {
        this.token = token.toString(); // here we receive a payload from the token and assigns it to our `user` variable
      }
    });
  }

  /**
   * Handle unauthorized error
   * @param {HttpRequest} request
   * @param {HttpHandler} next
   * @return {Observable}
   */
  /* private handle401Error(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<any> {
    if (!this.refreshTokenInProgress) {
      this.refreshTokenInProgress = true;
      this.refreshTokenSubject.next(null);

      return this.auth.authenticateUser().pipe(
        switchMap((token: AuthenticateUserResponse) => {
          this.refreshTokenInProgress = false;
          this.refreshTokenSubject.next(token.jwt);

          return next.handle(this.addToken(request, token.jwt));
        })
      );
    } else {
      return this.refreshTokenSubject.pipe(
        filter((token) => token != null),
        take(1),
        switchMap((jwt: string) => {
          return next.handle(this.addToken(request, jwt));
        })
      );
    }
  } */

  /**
   * Add token authorization to header request
   * @param {HttpRequest} request
   * @param {string} token
   * @return {HttpRequest}
   */
  private addToken(
    request: HttpRequest<any>,
    token: string
  ): HttpRequest<unknown> {
    return request.clone({
      setHeaders: {
        Authorization: `Bearer ${token}`,
      },
    });
  }

  /**
   * Intercept every request to add token authorization
   * @param {HttpRequest} request
   * @param {HttpHandler} next
   * @return {Observable}
   */
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<any> {
    this.auth.requestsPending += 1;

    // const token: string = localStorage.getItem("token");

    if (this.token) {
      request = this.addToken(request, this.token);
    }

    return next.handle(request).pipe(
      map((event) => {
        if (event instanceof HttpResponse) {
          this.auth.requestsPending -= 1;
          if (event.headers) {
            // Get digest for delete image from registry
            event.headers.get("Docker-Content-Digest");
          }
        }
        return event;
      }),
      catchError((error: HttpErrorResponse) => {
        this.auth.requestsPending -= 1;
        if (error instanceof HttpErrorResponse) {
          let message: string = "";
          switch (error.status) {
            case 401:
              this.router.navigate(["auth/login"]);
              break;
            case 422:
              message = `Impossible de contacter Portainer. Veuillez tenter d'effectuer 
                            la synchronisation du mot de passe de Portainer et rafraîchir cette page.`;
              break;
            case 409:
              message =
                "Un site avec le même nom existe déjà. Veuillez choisir un autre nom.";
              break;
            case 500:
              message =
                "Une erreur est survenue. Veuillez retenter l'opération.";
              console.error(error.error.details);
              break;
            default:
              return throwError(error);
          }
          if (message.length > 0) {
            this.toastr.error(message);
          }
        }
        return throwError(error);
      })
    );
  }
}
