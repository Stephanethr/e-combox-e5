/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable require-jsdoc */
import { Injectable, Optional, Inject } from "@angular/core";
import { ApiService } from "./api.service";
import { Observable } from "rxjs";
import { HttpParams, HttpClient } from "@angular/common/http";
import { Stack, StackAdapter } from "../models/stack";
import { BASE_PATH } from "./variables";
import { Configuration } from "./configuration";
import { CustomHttpUrlEncodingCodec } from "../../../encoder";
import { map, tap, share } from "rxjs/operators";
import { StackCreateRequest } from "../models/stack-create-request";
import { StackUpdateRequest } from "../models/stack-update-request";
import { StackFileResponse } from "../models/stack-file-response";
import { StackMigratePayload } from "../models/stack-migrate-payload";

@Injectable()
export class StacksService extends ApiService {
  public creationInProgress: boolean = false;

  /**
   * Constructor
   * @param {HttpClient} httpClient
   * @param {string} basePath Optional
   * @param {Configuration} configuration Optional
   * @param {StackAdapter} adapter
   * @param {ConstainerService} containerService
   */
  constructor(
    protected httpClient: HttpClient,
    @Optional() @Inject(BASE_PATH) basePath: string,
    @Optional() configuration: Configuration,
    private adapter: StackAdapter
  ) {
    super(httpClient, basePath, configuration);
  }

  /**
   * Deploy a new stack
   * Deploy a new stack into a Docker environment specified via the endpoint identifier.
   * **Access policy**: restricted
   * @param {number} type Stack deployment type. Possible values: 1 (Swarm stack) or 2 (Compose stack).
   * @param {string} method Stack deployment method. Possible values: file, string or repository.
   * @param {number} endpointId Identifier of the endpoint that will be used to deploy the stack.
   * @param {StackCreateRequest} body Stack details. Required when method equals string or repository.
   * @param {string} name Name of the stack. Required when method equals file.
   * @param {string} endpointID Endpoint identifier used to deploy the stack. Required when method equals file.
   * @param {string} swarmID Swarm cluster identifier. Required when method equals file and type equals 1.
   * @param {Blob} file Stack file. Required when method equals file.
   * @param {string} env Environment variables passed during deployment, represented as a JSON array
   * [{&#39;name&#39;: &#39;name&#39;, &#39;value&#39;: &#39;value&#39;}]. Optional, used when method equals file and
   * type equals 1.
   * @param {any} observe 'body'
   * @param {boolean} reportProgress false
   * @return {Observable<any>}
   */
  public stackCreate(
    type: number,
    method: string,
    endpointId: number,
    body?: StackCreateRequest,
    name?: string,
    endpointID?: string,
    swarmID?: string,
    file?: Blob,
    env?: string,
    observe: "body" = "body",
    reportProgress: boolean = false
  ): Observable<Stack> {
    this.creationInProgress = true;

    if (type === null || type === undefined) {
      throw new Error(
        "Required parameter type was null or undefined when calling stackCreate."
      );
    }

    if (method === null || method === undefined) {
      throw new Error(
        "Required parameter method was null or undefined when calling stackCreate."
      );
    }

    if (endpointId === null || endpointId === undefined) {
      throw new Error(
        "Required parameter endpointId was null or undefined when calling stackCreate."
      );
    }

    let queryParameters = new HttpParams({
      encoder: new CustomHttpUrlEncodingCodec(),
    });
    if (type !== undefined && type !== null) {
      queryParameters = queryParameters.set("type", <string>type.toString());
    }
    if (method !== undefined && method !== null) {
      queryParameters = queryParameters.set("method", <string>method);
    }
    if (endpointId !== undefined && endpointId !== null) {
      queryParameters = queryParameters.set(
        "endpointId",
        <string>endpointId.toString()
      );
    }

    let headers = this.configureDefaultHeaders();

    // to determine the Content-Type header
    const consumes: string[] = ["multipart/form-data"];
    const httpContentTypeSelected:
      | string
      | undefined = this.configuration.selectHeaderContentType(consumes);
    if (httpContentTypeSelected !== undefined) {
      headers = headers.set("Content-Type", httpContentTypeSelected);
    }

    const formParams: {
      append(param: string, value: string): void;
    } = new HttpParams({ encoder: new CustomHttpUrlEncodingCodec() });

    if (name !== undefined) {
      formParams.append("Name", <string>name);
    }
    if (endpointID !== undefined) {
      formParams.append("EndpointID", <string>endpointID);
    }
    if (swarmID !== undefined) {
      formParams.append("SwarmID", <string>swarmID);
    }
    if (file !== undefined) {
      formParams.append("file", <string>(<unknown>file));
    }
    if (env !== undefined) {
      formParams.append("Env", <string>env);
    }

    return this.httpClient
      .post<Stack>(`${this.basePath}/stacks`, body, {
        params: queryParameters,
        withCredentials: this.configuration.withCredentials,
        headers: headers,
        observe: observe,
        reportProgress: reportProgress,
      })
      .pipe(
        tap(
          () => {
            this.creationInProgress = false;
          },
          (error) => (this.creationInProgress = false)
        ),
        // Adapt each item in the raw data array
        map((data) => this.adapter.adapt(data)),
        share()
      );
  }

  /**
   * List stacks
   * List all stacks based on the current user authorizations.
   * Will return all stacks if using an administrator account otherwise it will only return the list of stacks
   * the user have access to. **Access policy**: restricted
   * @param {string} filters Filters to process on the stack list. Encoded as JSON (a map[string]string).
   * For example, {\&quot;SwarmID\&quot;: \&quot;jpofkc0i9uo9wtx1zesuk649w\&quot;} will only return stacks
   * that are part of the specified Swarm cluster. Available filters: EndpointID, SwarmID.
   * @param {any} observe set whether or not to return the data Observable as the body, response or events.
   * defaults to returning the body.
   * @param {boolean} reportProgress flag to report request and response progress.
   * @return {Observable<Stack[]>}
   */
  public stackList(
    filters?: string,
    observe: "body" = "body",
    reportProgress: boolean = false
  ): Observable<Stack[]> {
    let queryParameters = new HttpParams({
      encoder: new CustomHttpUrlEncodingCodec(),
    });
    if (filters !== undefined && filters !== null) {
      queryParameters = queryParameters.set("filters", <string>filters);
    }

    const headers = this.configureDefaultHeaders();

    return this.httpClient
      .get<Stack[]>(`${this.basePath}/stacks`, {
        params: queryParameters,
        withCredentials: this.configuration.withCredentials,
        headers: headers,
        observe: observe,
        reportProgress: reportProgress,
      })
      .pipe(
        // Adapt each item in the raw data array
        map((data) => data.map((item) => this.adapter.adapt(item)))
      );
  }

  /**
   * Update a stack
   * Update a stack. **Access policy**: restricted
   * @param id Stack identifier
   * @param body Stack details
   * @param endpointId Stacks created before version 1.18.0 might not have an associated endpoint identifier. Use this optional parameter to set the endpoint identifier used by the stack.
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   */
  public stackUpdate(
    id: number,
    body: StackUpdateRequest,
    endpointId?: number,
    observe?: "body",
    reportProgress?: boolean
  ): Observable<Stack>;
  // eslint-disable-next-line require-jsdoc
  public stackUpdate(
    id: number,
    body: StackUpdateRequest,
    endpointId: number = 1,
    observe: "body" = "body",
    reportProgress: boolean = false
  ): Observable<unknown> {
    if (id === null || id === undefined) {
      throw new Error(
        "Required parameter id was null or undefined when calling stackUpdate."
      );
    }

    if (body === null || body === undefined) {
      throw new Error(
        "Required parameter body was null or undefined when calling stackUpdate."
      );
    }

    let queryParameters = new HttpParams({
      encoder: new CustomHttpUrlEncodingCodec(),
    });
    if (endpointId !== undefined && endpointId !== null) {
      queryParameters = queryParameters.set(
        "endpointId",
        <string>endpointId.toString()
      );
    }

    const headers = this.configureDefaultHeaders();

    return this.httpClient
      .put<Stack>(
        `${this.basePath}/stacks/${encodeURIComponent(String(id))}`,
        body,
        {
          params: queryParameters,
          withCredentials: this.configuration.withCredentials,
          headers: headers,
          observe: observe,
          reportProgress: reportProgress,
        }
      )
      .pipe(
        // Adapt each item in the raw data array
        map((data) => this.adapter.adapt(data))
      );
  }

  /**
   * Stops a stopped Stack
   * Stops a stopped Stack. **Access policy**: restricted
   * @param {number} id Stack identifier
   * @param {string} observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param {boolean} reportProgress flag to report request and response progress.
   * @return {Observable<Stack>}
   */
  public stackStop(
    id: number,
    observe?: "body",
    reportProgress?: boolean
  ): Observable<Stack> {
    if (id === null || id === undefined) {
      throw new Error(
        "Required parameter id was null or undefined when calling stackStop."
      );
    }

    const headers = this.configureDefaultHeaders();

    return this.httpClient
      .post<Stack>(
        `${this.basePath}/stacks/${encodeURIComponent(String(id))}/stop`,
        null,
        {
          withCredentials: this.configuration.withCredentials,
          headers: headers,
          observe: observe,
          reportProgress: reportProgress,
        }
      )
      .pipe(map((data) => this.adapter.adapt(data)));
  }

  /**
   * Starts a stopped Stack
   * Starts a stopped Stack. **Access policy**: restricted
   * @param {number} id Stack identifier
   * @param {string} observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param {boolean} reportProgress flag to report request and response progress.
   * @return {Observable<Stack>}
   */
  public stackStart(
    id: number,
    observe?: "body",
    reportProgress?: boolean
  ): Observable<Stack> {
    if (id === null || id === undefined) {
      throw new Error(
        "Required parameter id was null or undefined when calling stackStart."
      );
    }

    const headers = this.configureDefaultHeaders();

    return this.httpClient
      .post<Stack>(
        `${this.basePath}/stacks/${encodeURIComponent(String(id))}/start`,
        null,
        {
          withCredentials: this.configuration.withCredentials,
          headers: headers,
          observe: observe,
          reportProgress: reportProgress,
        }
      )
      .pipe(map((data) => this.adapter.adapt(data)));
  }

  /**
   * Retrieve the content of the Stack file for the specified stack
   * Get Stack file content. **Access policy**: restricted
   * @param {number} id Stack identifier
   * @param {unknown} observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param {boolean} reportProgress flag to report request and response progress.
   * @return {Observable<StacksStackFileResponse>}
   */
  public stackFileInspect(
    id: number,
    observe?: "body",
    reportProgress?: boolean
  ): Observable<StackFileResponse> {
    if (id === null || id === undefined) {
      throw new Error(
        "Required parameter id was null or undefined when calling stackFileInspect."
      );
    }

    const headers = this.configureDefaultHeaders();

    return this.httpClient.get<StackFileResponse>(
      `${this.basePath}/stacks/${encodeURIComponent(String(id))}/file`,
      {
        withCredentials: this.configuration.withCredentials,
        headers: headers,
        observe: observe,
        reportProgress: reportProgress,
      }
    );
  }

  /**
   * Remove a stack
   * Remove a stack. **Access policy**: restricted
   * @param {number} id Stack identifier
   * @param {number} endpointId Endpoint identifier used to remove an external stack (required when external is set to true)
   * @param {boolean} external Set to true to delete an external stack. Only external Swarm stacks are supported
   * @param {unknown} observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param {boolean} reportProgress flag to report request and response progress.
   * @return {Observable<void>}
   */
  public stackDelete(
    id: number,
    endpointId: number = 1,
    external?: boolean,
    observe?: "body",
    reportProgress?: boolean
  ): Observable<void> {
    if (id === null || id === undefined) {
      throw new Error(
        "Required parameter id was null or undefined when calling stackDelete."
      );
    }

    let queryParameters = new HttpParams({
      encoder: new CustomHttpUrlEncodingCodec(),
    });
    if (external !== undefined && external !== null) {
      queryParameters = queryParameters.set("external", <any>external);
    }
    if (endpointId !== undefined && endpointId !== null) {
      queryParameters = queryParameters.set("endpointId", <any>endpointId);
    }

    const headers = this.configureDefaultHeaders();

    return this.httpClient.delete<any>(
      `${this.basePath}/stacks/${encodeURIComponent(String(id))}`,
      {
        params: queryParameters,
        withCredentials: this.configuration.withCredentials,
        headers: headers,
        observe: observe,
        reportProgress: reportProgress,
      }
    );
  }

  /**
   * Duplicate a stack
   * Duplicate a stack. It will re-create the stack inside the target endpoint before removing the original stack. **Access policy**: restricted
   * @param {number} id Stack identifier
   * @param {StacksStackMigratePayload} body Stack migration details
   * @param {any} observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param {boolean} reportProgress flag to report request and response progress.
   * @return {Observable<Stack>}
   */
  public stackDuplicate(
    id: number,
    body: StackMigratePayload,
    observe: any = "body",
    reportProgress: boolean = false
  ): Observable<Stack> {
    if (id === null || id === undefined) {
      throw new Error(
        "Required parameter id was null or undefined when calling stackMigrate."
      );
    }

    if (body === null || body === undefined) {
      throw new Error(
        "Required parameter body was null or undefined when calling stackMigrate."
      );
    }
    let queryParameters = new HttpParams({
      encoder: new CustomHttpUrlEncodingCodec(),
    });
    queryParameters = queryParameters.set("endpointId", <any>1);

    const headers = this.configureDefaultHeaders();

    return this.httpClient
      .post<Stack>(
        `${this.basePath}/stacks/${encodeURIComponent(String(id))}/migrate`,
        body,
        {
          params: queryParameters,
          withCredentials: this.configuration.withCredentials,
          headers: headers,
          observe: observe,
          reportProgress: reportProgress,
        }
      )
      .pipe(map((data) => this.adapter.adapt(data)));
  }
}
