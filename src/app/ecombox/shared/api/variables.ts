import { InjectionToken } from "@angular/core";

export const BASE_PATH = new InjectionToken<string>("basePath");

export const API_URL: string = "https://10.211.55.5:8800/api";
export const PORTAINER_URL: string = API_URL.slice(0, -3);

export const COLLECTION_FORMATS = {
  csv: ",",
  tsv: "   ",
  ssv: " ",
  pipes: "|",
};

export const DB_TYPE: string = "-db-";

export type TAB_SERVERS =
  | "blog"
  | "woocommerce"
  | "odoo"
  | "prestashop"
  | "humhub"
  | "mautic"
  | "suitecrm"
  | "kanboard"
  | "sftp"
  | "pma";

export const SERVER_TYPES: Record<TAB_SERVERS, string> = {
  blog: "blog",
  woocommerce: "woocommerce",
  odoo: "odoo",
  prestashop: "prestashop",
  humhub: "humhub",
  mautic: "mautic",
  suitecrm: "suitecrm",
  kanboard: "kanboard",
  sftp: "sftp",
  pma: "pma",
};

export const SERVER_WITH_PMA: Record<TAB_SERVERS, boolean> = {
  blog: true,
  woocommerce: true,
  odoo: false,
  prestashop: true,
  humhub: false,
  mautic: false,
  suitecrm: false,
  kanboard: false,
  sftp: false,
  pma: false,
};

export const SERVER_WITH_SFTP: Record<TAB_SERVERS, boolean> = {
  blog: false,
  woocommerce: false,
  odoo: true,
  prestashop: true,
  humhub: false,
  mautic: false,
  suitecrm: false,
  kanboard: false,
  sftp: false,
  pma: false,
};

export const USERS_DB: Record<TAB_SERVERS, string> = {
  blog: "userWP",
  woocommerce: "userWP",
  odoo: "userOdoo",
  prestashop: "userPS",
  humhub: "userHH",
  mautic: "userMautic",
  suitecrm: "userSuiteCRM",
  kanboard: "userKB",
  sftp: "",
  pma: "",
};

export const IMAGES: Record<TAB_SERVERS, string> = {
  blog: "reseaucerta/blog-avec-wp-cli:dev",
  woocommerce: "reseaucerta/woocommerce-vierge-avec-wp-cli:dev",
  odoo: "reseaucerta/odoo14:3.0",
  prestashop: "reseaucerta/prestashop-vierge:dev",
  humhub: "reseaucerta/humhub:dev",
  mautic: "reseaucerta/mautic:v3",
  suitecrm: "reseaucerta/suitecrm:dev",
  kanboard: "reseaucerta/kanboard:dev",
  sftp: "sftpgo",
  pma: "pma",
};

export const BACK_OFFICE = {
  blog: "/wp-admin",
  woocommerce: "/wp-admin",
  odoo: "/web/database/selector",
  prestashop: "/administration",
  humhub: "",
  mautic: "",
  suitecrm: "",
  kanboard: "",
};

export const DOCKER_COMPOSE = "docker-compose-";
export const COMPOSE_EXT = ".yml";

export type DB_TYPE =
  | "vierge"
  | "perso"
  | "v12"
  | "v13"
  | "v14"
  | "primeur"
  | "surplomb"
  | "generik"
  | "pepinieres"
  | "ada"
  | "bddev"
  | "other";

export const PARAM_STACK_FILE: Record<DB_TYPE, string> = {
  other: "",
  vierge: "-vierge",
  perso: "-art",
  v12: "-12",
  v13: "-13",
  v14: "-14",
  primeur: "-primeur",
  surplomb: "-surplomb",
  generik: "-generik",
  pepinieres: "-pepinieres",
  ada: "-ada",
  bddev: "-bddev",
};

export const PARAM_SUFFIX_COMPOSE: Record<DB_TYPE, string> = {
  other: "",
  vierge: "",
  perso: "art-",
  v12: "12-",
  v13: "13-",
  v14: "14-",
  primeur: "primeur-",
  surplomb: "surplomb-",
  generik: "generik-",
  pepinieres: "pepinieres-",
  ada: "ada-",
  bddev: "bddev-",
};

export const PARAM_OPTION: Record<DB_TYPE, string> = {
  other: "other",
  vierge: "vierge",
  perso: "perso",
  v12: "v12",
  v13: "v13",
  v14: "v14",
  primeur: "primeur",
  surplomb: "surplomb",
  generik: "generik",
  pepinieres: "pepinieres",
  ada: "ada",
  bddev: "bddev",
};

export const PARAM_STACK_NAME: Record<DB_TYPE, string> = {
  other: "",
  vierge: "",
  perso: "art-",
  v12: "12-",
  v13: "13-",
  v14: "14-",
  primeur: "primeur-",
  surplomb: "surplomb-",
  generik: "generik-",
  pepinieres: "pepinieres-",
  ada: "ada-",
  bddev: "bddev-",
};

export const GET_PARAM_STACK = {
  type: 2,
  method: "repository",
  endpointID: 1,
  // repositoryURL: "https://gitlab.com/siollb/e-comBox_docker-compose_siollb",
  // repositoryReferenceName: "refs/heads/master",
  repositoryURL: "https://gitlab.com/e-combox/e-comBox_docker-compose",
  repositoryReferenceName: "refs/heads/v4",
};

export const DATA_USAGE = {
  imageSize: "Size",
  containerSize: "SizeRootFs",
};

export const CMD_SCRIPT: string = "/tmp/config-site.sh";
