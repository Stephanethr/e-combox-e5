/* eslint-disable @typescript-eslint/no-explicit-any */
import { Inject, Injectable, Optional } from "@angular/core";
import { Observable } from "rxjs";
import { ApiService } from "./api.service";
import { Image } from "../models/image";
import { CustomHttpUrlEncodingCodec } from "../../../encoder";
import { HttpClient, HttpParams } from "@angular/common/http";
import { map, mergeMap } from "rxjs/operators";
import { Configuration } from "./configuration";
import { BASE_PATH, SERVER_TYPES } from "./variables";
import { ContainerService } from "./container.service";

@Injectable()
export class ImageService extends ApiService {
  private endpoint: string;

  /**
   * Constructor
   * @param {HttpClient} httpClient
   * @param {ContainerService} containerService
   * @param {string} basePath Optional
   * @param {Configuration} configuration Optional
   * @param {ContainerAdapter} adapter
   */
  constructor(
    protected httpClient: HttpClient,
    private containerService: ContainerService,
    @Optional() @Inject(BASE_PATH) basePath: string,
    @Optional() configuration: Configuration
  ) {
    super(httpClient, basePath, configuration);
    this.endpoint = `${this.basePath}/endpoints/1/docker`;
  }

  /**
   * @param {string} nameContainer
   * @param {string} serverType
   * @param {string} tagImage
   * @param {string} portRepo
   * @return {Observable<Image>}
   */
  public imageCreate(
    nameContainer: string,
    serverType: string,
    tagImage: string,
    portRepo: string
  ): Observable<any> {
    const cmd = "/tmp/prepare-image.sh";
    const nameImage = `localhost:${portRepo}%2F${serverType}`;
    let execOpt = false;
    if (serverType === SERVER_TYPES.odoo) {
      execOpt = true;
    }

    return this.containerService
      .runExecInstance(nameContainer, cmd, execOpt)
      .pipe(
        mergeMap(() =>
          this.imageCommit(nameContainer, nameImage, tagImage).pipe(
            mergeMap(() => this.imagePush(nameImage + ":" + tagImage))
          )
        )
      );
  }

  /**
   * @param {string} nameContainer
   * @param {string} serverType
   * @param {string} tagImage
   * @param {string} portRepo
   * @return {Observable<Image>}
   */
  public imageCreateDB(
    nameContainer: string,
    serverType: string,
    tagImage: string,
    portRepo: string
  ): Observable<any> {
    const sqlFile = "/docker-entrypoint-initdb.d/" + serverType + "-db.sql";
    let cmd;

    if (serverType === SERVER_TYPES.odoo) {
      /* DO NOT TOUCH !! */
      cmd = "pg_dumpall -f " + sqlFile;
      cmd += "&& sed -i '14,15s/.*//' " + sqlFile;
      cmd +=
        "&& sed -i '5s/.*/\\\\set passwd `echo \"\\$DB_PASS\"`/' " + sqlFile;
      cmd +=
        '&& sed -i "15s/.*/ALTER USER \\"userOdoo\\" WITH PASSWORD :\'passwd\';/" ' +
        sqlFile;
    } else {
      cmd = `mysqldump -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE > ${sqlFile}`;
    }

    const nameImage = `localhost:${portRepo}%2F${serverType}-db`;

    return this.containerService
      .runExecInstance(nameContainer, cmd)
      .pipe(
        mergeMap(() =>
          this.imageCommit(nameContainer, nameImage, tagImage).pipe(
            mergeMap(() => this.imagePush(nameImage + ":" + tagImage))
          )
        )
      );
  }
  /**
   * @param {string} id Id of the container used for image creation
   * @param {string} name Name of the new image tag
   * @param {string} tag
   * @return {Observable<any>}
   */
  public imageCommit(id: string, name: string, tag: string): Observable<Image> {
    /* let queryParameters = new HttpParams({
      encoder: new CustomHttpUrlEncodingCodec(),
    });*/

    // queryParameters = queryParameters.set("container", id);
    // queryParameters = queryParameters.set("repo", name);
    name = name.replace("%2F", "/");
    const headers = this.configureDefaultHeaders();

    return this.httpClient.post<Image>(
      `${this.endpoint}/commit?container=${id}&repo=${name}&tag=${tag}`,
      {
        // params: queryParameters,
        withCredentials: this.configuration.withCredentials,
        headers: headers,
      }
    );
  }

  /**
   * @param {string} name Name of the image to push
   * @return {Observable<any>}
   */
  public imagePush(name: string): Observable<any> {
    const headers = this.configureDefaultHeaders();
    name = name.replace("%2F", "/");

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    return this.httpClient
      .post<any>(`${this.endpoint}/images/${name}/push`, {
        withCredentials: this.configuration.withCredentials,
        headers: headers,
        reportProgress: false,
      })
      .pipe(map((result: any) => console.dir(result)));
  }

  /**
   * @param {string} filters
   * @return {Observable<any>}
   */
  public imagesList(filters?: string): Observable<Image> {
    let queryParameters = new HttpParams({
      encoder: new CustomHttpUrlEncodingCodec(),
    });

    if (filters !== undefined && filters !== null) {
      queryParameters = queryParameters.set("filters", <string>filters);
    }

    const headers = this.configureDefaultHeaders();

    return this.httpClient.get<Image>(`${this.basePath}${this.endpoint}`, {
      params: queryParameters,
      withCredentials: this.configuration.withCredentials,
      headers: headers,
      observe: "body",
      reportProgress: false,
    });
  }
}
