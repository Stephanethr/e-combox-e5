/* eslint-disable @typescript-eslint/no-explicit-any */
import { Injectable } from "@angular/core";
import { ApiService } from "./api.service";
import { Observable } from "rxjs";
import { HttpParams } from "@angular/common/http";
import { CustomHttpUrlEncodingCodec } from "../../../encoder";

@Injectable()
export class VolumeService extends ApiService {
  private endpoint: string = `/endpoints/1/docker/volumes`;

  /**
   * @param {string} filters
   * @return {Observable<any>}
   */
  public volumesList(filters?: string): Observable<any> {
    let queryParameters = new HttpParams({
      encoder: new CustomHttpUrlEncodingCodec(),
    });

    if (filters !== undefined && filters !== null) {
      queryParameters = queryParameters.set("filters", <string>filters);
    }

    const headers = this.configureDefaultHeaders();

    return this.httpClient.get<any>(`${this.basePath}${this.endpoint}`, {
      params: queryParameters,
      withCredentials: this.configuration.withCredentials,
      headers: headers,
      observe: "body",
      reportProgress: false,
    });
  }

  /**
   * @param {string} name name's volume to remove
   * @return {Observable<any>}
   */
  public volumeRemove(name: string): Observable<any> {
    const headers = this.configureDefaultHeaders();

    return this.httpClient.delete<any>(
      `${this.basePath}${this.endpoint}/${encodeURIComponent(name)}`,
      {
        params: null,
        withCredentials: this.configuration.withCredentials,
        headers: headers,
        observe: "body",
        reportProgress: false,
      }
    );
  }

  /**
   *
   * @return {Observable<unknown>}
   */
  public volumesRemoveUnused(): Observable<unknown> {
    const headers = this.configureDefaultHeaders();

    return this.httpClient.post<unknown>(
      `${this.basePath}${this.endpoint}/prune`,
      {
        withCredentials: this.configuration.withCredentials,
        headers: headers,
      }
    );
  }
}
