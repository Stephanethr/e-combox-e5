import { Injectable } from "@angular/core";
import { ApiService } from "./api.service";
import { User } from "../models/user";
import { Observable } from "rxjs";
// import { Observable } from "rxjs";
// import { tap, catchError } from "rxjs/operators";
// import { AuthenticateUserResponse } from "../models/authenticate-user-response";

@Injectable({
  providedIn: "root",
})
export class AuthService extends ApiService {
  public defaultUser: User = {
    username: "admin",
    // password: "portnair$$@DMIN",
    password: "portnairAdmin",
    jwt: "",
  };

  public requestsPending: number = 0;

  /**
   * @param {number} id
   * @param {string} newPasswd
   * @param {string} currentPasswd
   * @return {Observable<void>}
   */
  public changePasswd(
    id: number,
    newPasswd: string,
    currentPasswd: string
  ): Observable<void> {
    // const headers = this.configureDefaultHeaders();

    // const body = JSON.stringify(passwd);

    return this.httpClient.put<void>(`${this.basePath}/users/${id}/passwd`, {
      newPassword: newPasswd,
      password: currentPasswd,
    });
  }

  /**
   * Init API session
   * @param {User} body
   * @param {any} observe
   * @param {boolean} reportProgress false
   * @return {Observable<HttpResponse<User>>}
   */
  /* public authenticateUser(
    body: User = this.defaultUser,
    observe: "body" = "body",
    reportProgress: boolean = false
  ): Observable<AuthenticateUserResponse> {
    if (body === null || body === undefined) {
      throw new Error(
        "Required parameter body was null or undefined when calling authenticateUser."
      );
    }

    const headers = this.configureDefaultHeaders();

    return this.httpClient
      .post<AuthenticateUserResponse>(`${this.basePath}/auth`, body, {
        withCredentials: this.configuration.withCredentials,
        headers: headers,
        observe: observe,
        reportProgress: reportProgress,
      })
      .pipe(
        tap((token) => {
          localStorage.setItem("token", token.jwt);
        }),
        catchError(this.handleError("Authenticate"))
      );
  }*/
}
