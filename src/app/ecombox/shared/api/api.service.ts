import { Optional, Inject, Injectable } from "@angular/core";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { Configuration } from "./configuration";
import { BASE_PATH, API_URL } from "./variables";
import { Observable, of } from "rxjs";

@Injectable()
export class ApiService {
  public basePath = API_URL;
  public defaultHeaders = new HttpHeaders();
  public configuration = new Configuration();

  /**
   *
   * @param {HttpClient} httpClient
   * @param {string} basePath Optional
   * @param {Configuration} configuration Optional
   */
  constructor(
    protected httpClient: HttpClient,
    @Optional() @Inject(BASE_PATH) basePath?: string,
    @Optional() configuration?: Configuration
  ) {
    if (basePath) {
      this.basePath = basePath;
    }
    if (configuration) {
      this.configuration = configuration;
      this.basePath = basePath || configuration.basePath || this.basePath;
    }
  }

  /**
   * @param {string[]} consumes  mime-types
   * @return {boolean} true: consumes contains 'multipart/form-data', false: otherwise
   */
  protected canConsumeForm(consumes: string[]): boolean {
    const form = "multipart/form-data";
    for (const consume of consumes) {
      if (form === consume) {
        return true;
      }
    }
    return false;
  }

  /**
   * Configure headers request
   * @return {HttpHeaders}
   */
  protected configureDefaultHeaders(): HttpHeaders {
    let headers = this.defaultHeaders;

    const httpHeaderAccepts: string[] = [
      "application/json",
      "text/html",
      "application/xhtml+xml",
      "application/vnd.docker.distribution.manifest.v2+json",
      "*/*",
    ];
    const httpHeaderAcceptSelected:
      | string
      | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
    if (httpHeaderAcceptSelected !== undefined) {
      headers = headers.set("Accept", httpHeaderAcceptSelected);
    }

    const consumes: string[] = [
      "application/json",
      "charset=utf-8",
      "application/x-www-form-urlencoded",
    ];
    const httpContentTypeSelected:
      | string
      | undefined = this.configuration.selectHeaderContentType(consumes);
    if (httpContentTypeSelected !== undefined) {
      headers = headers.set("Content-Type", httpContentTypeSelected);
    }

    return headers;
  }

  /**
   * Handle errors request
   * @param {string} operation
   * @param {T} result
   * @return {Observable}
   */
  protected handleError<T>(operation = "operation", result?: T) {
    return (error): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.error(`${operation} failed: ${error.message}`);
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
