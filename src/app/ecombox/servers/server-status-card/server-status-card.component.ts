/* eslint-disable new-cap */
import { Container } from "../../shared/models/container";
import { Component, Input, OnInit, ViewChild } from "@angular/core";
import { Stack } from "../../shared/models/stack";
import { StackStore } from "../../shared/store/stack.store";
import { NbContextMenuDirective, NbDialogService } from "@nebular/theme";
import { DialogNamePromptComponent } from "../dialog-name-prompt/dialog-name-prompt.component";
import { SERVER_TYPES } from "../../shared/api/variables";
import { DialogTemplateComponent } from "../dialog-template/dialog-template.component";
import { HttpErrorResponse } from "@angular/common/http";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "ecx-server-status-card",
  templateUrl: "./server-status-card.component.html",
  styleUrls: ["./server-status-card.component.scss"],
})
export class ServerStatusCardComponent implements OnInit {
  @ViewChild(NbContextMenuDirective) contextMenu: NbContextMenuDirective;

  items = [
    { title: "Arrêter/Démarrer" },
    { title: "Supprimer" },
    { title: "Dupliquer" },
  ];

  @Input() title: string;
  @Input() type: string;
  @Input() stackID: number;
  @Input() on: boolean;

  serverType: string;
  stack: Stack;
  mainContainer: Container;
  loading: boolean = false;
  urlBack: string = "";
  valid: string;
  password: string = "";
  tag: string = "";

  /**
   *
   */
  constructor(
    private store: StackStore,
    private dialogService: NbDialogService,
    private toastr: ToastrService
  ) {
    this.toastr.toastrConfig.timeOut = 10000;
  }

  /**
   *
   */
  ngOnInit(): void {
    this.stack = this.store.getStack(this.stackID);
    this.serverType = this.stack.getServerType();
    if (this.stack.getStatus() === 1) {
      this.mainContainer = this.stack.getMainContainer();
      if (this.mainContainer !== undefined) {
        this.urlBack = this.mainContainer.getUrlBackOffice();
        if (this.serverType === SERVER_TYPES.mautic) {
          // Display password for mautic servers
          this.password = this.stack
            .getEnvVars()
            .find((e) => e.name === "DB_PASS").value;
        }
      }
    }
    this.tag = this.stack.getEnvVars().find((e) => e.name === "TAG")?.value;
  }

  /**
   *
   */
  onClick(): void {
    this.loading = true;
    if (this.on) {
      this.stop();
    } else {
      this.start();
    }
  }

  /**
   *
   */
  confirmDel(): void {
    this.dialogService
      .open(DialogNamePromptComponent, {
        context: {
          message:
            "Cette action supprimera définitivement toutes les données associées à ce site",
        },
      })
      .onClose.subscribe((name) => {
        this.valid = name;
        if (name === "oui") {
          this.loading = true;
          this.store.deleteStack(this.stackID, this.stack.getName()).subscribe(
            () => {
              this.loading = false;
            },
            (error: HttpErrorResponse) => {
              this.loading = false;
              if (error.status === 404) {
                this.toastr.warning(
                  `Une erreur est survenue, veuillez retenter l'opération dans quelques minutes ou utiliser l'accès admin`
                );
              }
            }
          );
          this.store.deleteLinkedStack(this.stack);
        }
      });
  }

  /**
   * @TODO
   */
  onDuplicate(): void {
    this.dialogService.open(DialogTemplateComponent, {
      context: {
        stack: this.stack,
      },
      closeOnBackdropClick: false,
    });
  }

  /**
   *
   */
  private start(): void {
    this.store.updateStack(this.stackID, this.serverType).subscribe(
      () => {
        // 2 seconds of delay for nginx
        setTimeout(() => {
          this.loading = false;
        }, 2000);
      },
      (error: Error) => {
        console.error(error.message);
        this.loading = false;
      }
    );
  }

  /**
   *
   */
  private stop(): void {
    this.store.stopStack(this.stackID).subscribe(() => {
      this.loading = false;
    });
  }
}
