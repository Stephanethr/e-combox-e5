import { Component, Input } from "@angular/core";
import { NbDialogRef } from "@nebular/theme";

@Component({
  selector: "ecx-dialog-name-prompt",
  templateUrl: "dialog-name-prompt.component.html",
  styleUrls: ["dialog-name-prompt.component.scss"],
})
export class DialogNamePromptComponent {
  @Input() message: string;
  /**
   *
   * @param {NbDialogRef} ref
   */
  constructor(protected ref: NbDialogRef<DialogNamePromptComponent>) {}

  err: string;

  /**
   *
   */
  cancel(): void {
    this.ref.close();
  }

  /**
   *
   * @param {string} name
   */
  submit(name: string): void {
    if (name === "" || name !== "oui") {
      this.err = "Saisir 'oui' pour confirmer la suppression";
    } else {
      this.ref.close(name);
    }
  }
}
