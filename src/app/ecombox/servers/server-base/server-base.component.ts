import { Component } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "ngx-server-base",
  styleUrls: ["./server-base.component.scss"],
  templateUrl: "./server-base.component.html",
})
export class ServerBaseComponent {
  serverType: string;

  /**
   * Constructor
   * @param {ActivatedRoute} actRoute
   * @param {TranslateService} translate
   */
  constructor(private actRoute: ActivatedRoute, translate: TranslateService) {
    this.serverType = this.actRoute.snapshot.url.toString();
  }
}
