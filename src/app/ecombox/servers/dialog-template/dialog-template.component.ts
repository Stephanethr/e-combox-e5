import { HttpErrorResponse } from "@angular/common/http";
import { Input, OnInit } from "@angular/core";
import { Component } from "@angular/core";
import { NbDialogRef } from "@nebular/theme";
import { ToastrService } from "ngx-toastr";
import { DockerRegistryService } from "../../shared/api/docker-registry.service";
import { ImageService } from "../../shared/api/image.service";
import { Stack } from "../../shared/models/stack";
import { StackStore } from "../../shared/store/stack.store";

@Component({
  selector: "ecx-dialog-template",
  templateUrl: "dialog-template.component.html",
  styleUrls: ["dialog-template.component.scss"],
})
export class DialogTemplateComponent implements OnInit {
  @Input() stack: Stack;
  loading = false;
  listImages = [];
  img: { label: string; value: string };
  itemSelected: string;
  err: string;

  /**
   *
   * @param {NbDialogRef} ref
   */
  constructor(
    protected ref: NbDialogRef<DialogTemplateComponent>,
    private imageService: ImageService,
    private toast: ToastrService,
    private store: StackStore,
    private registryService: DockerRegistryService
  ) {}

  /**
   *
   */
  ngOnInit(): void {
    let domain: string = "";
    let port: string = null;
    if (this.store.getUseReverseProxy() === "O") {
      domain = this.store.getFQDN();
    } else {
      if (
        this.store.getPublicURL() !== "" &&
        this.store.getPublicURL() !== undefined
      ) {
        domain = this.store.getPublicURL();
      } else {
        domain = this.store.getDockerIP();
      }

      port = this.store.getNginxPort();
    }
    this.registryService
      .registryTags(this.stack.getServerType(), domain, port)
      .subscribe(
        (result) => {
          if (result.tags !== null) {
            result.tags.forEach((element) => {
              this.listImages.push({
                value: element,
                label: element,
              });
            });
          }
        },
        (error: HttpErrorResponse) => {}
      );
  }

  /**
   *
   */
  cancel(): void {
    this.ref.close();
  }

  /**
   *
   * @param {string} name
   */
  submit(name: string): void {
    if (name === undefined || name === "") {
      if (this.itemSelected === undefined || this.itemSelected === "") {
        this.err = "Veuillez saisir un nom ou choisir un modèle existant";
        return;
      } else {
        name = this.itemSelected;
      }
    }

    const regex = RegExp("^[a-z0-9]+$");
    if (!regex.test(name)) {
      this.err =
        "Le nom du modèle ne peut contenir que des minuscules et des chiffres";
      return;
    }

    this.loading = true;
    this.imageService
      .imageCreate(
        this.stack.getMainContainer().getName(),
        this.stack.getServerType(),
        name,
        this.store.getResgistryPort()
      )
      .subscribe(
        () => {},
        (error: HttpErrorResponse) => {
          if (error.status === 200) {
            this.imageService
              .imageCreateDB(
                this.stack.getDbContainer().getName(),
                this.stack.getServerType(),
                name,
                this.store.getResgistryPort()
              )
              .subscribe(
                () => {},
                (error: HttpErrorResponse) => {
                  this.loading = false;
                  if (error.status === 200) {
                    this.store.messageSource.next("update");
                    this.ref.close();
                    this.toast.success(
                      `Votre modèle ${name} a été créé ! Vous pouvez maintenant créer des sites ${this.stack.getServerType()} à partir de ce dernier`
                    );
                  } else {
                    console.dir(error.error);
                  }
                }
              );
          } else {
            this.loading = false;
            console.dir(error);
          }
        }
      );
  }
}
