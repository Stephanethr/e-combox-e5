import { HttpErrorResponse } from "@angular/common/http";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
} from "@angular/core";
import { Router } from "@angular/router";
import {
  NbAuthJWTToken,
  NbAuthService,
  NbResetPasswordComponent,
} from "@nebular/auth";
import { AuthService } from "../shared/api/auth.service";
import { StackStore } from "../shared/store/stack.store";

@Component({
  selector: "ecx-profile",
  templateUrl: "./profile.component.html",
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfileComponent extends NbResetPasswordComponent {
  public currentUser = {
    id: 0,
  };

  public minLength: number;
  public maxLength: number;

  /**
   *
   * @param {AuthService} authService
   * @param {NbAuthService} service
   * @param {ChangeDetectorRef} cd
   * @param {Router} router
   */
  constructor(
    protected authService: AuthService,
    protected service: NbAuthService,
    protected cd: ChangeDetectorRef,
    protected router: Router,
    private store: StackStore
  ) {
    super(service, {}, cd, router);
    this.showMessages = {};
    this.maxLength = 15;
    this.minLength = 12;
    this.redirectDelay = 2000;
    this.service.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      if (token.isValid()) {
        this.currentUser = token.getPayload(); // here we receive a payload from the token and assigns it to our `user` variable
      }
    });
  }
  /**
   *
   */
  public resetPass(): void {
    this.errors = [];
    this.messages = [];
    this.submitted = true;

    this.authService
      .changePasswd(
        this.currentUser.id,
        this.user.password,
        this.user.curpassword
      )
      .subscribe(
        () => {
          this.submitted = false;
          this.messages.push(
            "Votre mot de passe a été modifié ! Vous allez être déconnecté afin de vous identifier avec votre nouveau mot de passe."
          );

          setTimeout(() => {
            return this.service.logout("username").subscribe((result) => {
              if (result.isSuccess) {
                this.store.unloadData();
                this.router.navigate([result.getRedirect()]);
              }
            });
          }, this.redirectDelay);

          this.cd.detectChanges();
          console.debug("OK");
        },
        (error: HttpErrorResponse) => {
          this.submitted = false;
          if (error.status === 400) {
            this.errors.push(error.message);
          } else if (error.status === 403) {
            this.errors.push("Le mot de passe actuel n'est pas correct");
          }
          console.dir(this.messages);
          this.cd.detectChanges();
        }
      );
  }
}
