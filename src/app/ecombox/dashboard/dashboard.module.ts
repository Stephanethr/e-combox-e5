import { NgModule } from "@angular/core";
import { ThemeModule } from "../../@theme/theme.module";
import { NgxEchartsModule } from "ngx-echarts";
import { TranslateModule } from "@ngx-translate/core";
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbProgressBarModule,
  NbListModule,
  NbIconModule,
  NbSpinnerModule,
} from "@nebular/theme";

import { DashboardComponent } from "./dashboard.component";
import { EcomboxStateComponent } from "./ecombox-state/ecombox-state.component";
import { EcomboxServerDetailsComponent } from "./ecombox-server-details/ecombox-server-details.component";
import { EcomboxServerPieComponent } from "./ecombox-server-pie/ecombox-server-pie.component";
import { EcomboxUsageStatsComponent } from "./ecombox-usage-stats/ecombox-usage-stats.component";

@NgModule({
  imports: [
    ThemeModule,
    NgxEchartsModule,
    NbActionsModule,
    NbButtonModule,
    NbCardModule,
    NbProgressBarModule,
    NbListModule,
    NbIconModule,
    NbSpinnerModule,
    TranslateModule.forChild({
      extend: true,
    }),
  ],
  declarations: [
    DashboardComponent,
    EcomboxStateComponent,
    EcomboxServerDetailsComponent,
    EcomboxServerPieComponent,
    EcomboxUsageStatsComponent,
  ],
})
export class DashboardModule {}
