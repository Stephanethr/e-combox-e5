import { Component, OnDestroy } from "@angular/core";
import { Subscription } from "rxjs";
import { ContainerService } from "../../shared/api/container.service";
import { SystemService } from "../../shared/api/system.service";
import { StackStore } from "../../shared/store/stack.store";
import { Utils } from "../../shared/utils";

@Component({
  selector: "ecx-ecombox-usage-stats",
  templateUrl: "./ecombox-usage-stats.component.html",
  styleUrls: ["./ecombox-usage-stats.component.scss"],
})
export class EcomboxUsageStatsComponent implements OnDestroy {
  private subscription: Subscription;

  loading = false;
  loadingSpace = false;
  display = false;

  diskSpaceDescription: string;
  diskSpaceUsed: number;
  memoryUsed: number;
  cpuUsed: number;

  /**
   * @param {StackStore} stackStore
   * @param {SystemService} systemService
   */
  constructor(
    private sytemeService: SystemService,
    private containerService: ContainerService,
    private store: StackStore
  ) {}

  /**
   *
   */
  initVariables(): void {
    this.memoryUsed = 0.0;
    this.memoryUsed = 0.0;
    this.cpuUsed = 0.0;
    this.diskSpaceDescription = "Calcul en cours";
    this.diskSpaceUsed = 0;
    this.loading = true;
    this.loadingSpace = true;
  }

  /**
   * No Param - void return
   */
  onRefresh(): void {
    this.initVariables();
    this.getDiskSpace();
    this.getMemCPU();
  }

  /**
   *
   */
  private getDiskSpace(): void {
    this.sytemeService.systemGetDataUsage().subscribe(
      (response) => {
        if (response) {
          // disk space used by images
          if (response.LayersSize) {
            this.diskSpaceUsed += response.LayersSize;
          }
          // disk space used by volumes
          if (response.Volumes) {
            response.Volumes.forEach((element) => {
              this.diskSpaceUsed += element.UsageData.Size;
            });
          }
          this.diskSpaceDescription = Utils.formatBytes(this.diskSpaceUsed);
        } else {
          this.diskSpaceDescription = "Impossible de récupérer l'espace disque";
        }
        this.loading = false;
        this.loadingSpace = false;
        this.display = true;
      },
      (error: Error) => {
        console.error(error.message);
        this.loading = false;
        this.loadingSpace = false;
        this.display = false;
      }
    );
  }

  /**
   *
   */
  private getMemCPU(): void {
    this.subscription = this.store.getStacks().subscribe((stacks) => {
      const ids: string[] = [];
      stacks.forEach((stack) => {
        if (stack.getContainers() !== undefined) {
          stack.getContainers().forEach((container) => {
            ids.push(container.getID());
          });
        }
      });
      this.containerService.containerStats(ids).subscribe(
        (result) => {
          result.forEach((stats) => {
            if (stats.cpu_stats) {
              this.cpuUsed += Utils.calculCPUUsage(
                stats.cpu_stats,
                stats.precpu_stats
              );
            }
            if (stats.memory_stats.limit) {
              this.memoryUsed +=
                (stats.memory_stats.usage * 100) / stats.memory_stats.limit;
            }
          });
          this.loading = false;
          this.display = true;
        },
        (error: Error) => {
          console.error(error.message);
          this.loading = false;
          this.display = false;
        }
      );
    });
  }

  /**
   *
   */
  ngOnDestroy(): void {
    if (this.subscription !== undefined) {
      this.subscription.unsubscribe();
    }
  }
}
