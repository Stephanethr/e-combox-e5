import { Component, OnDestroy, OnInit } from "@angular/core";
import { StackStore } from "../../shared/store/stack.store";
import { Subscription } from "rxjs";
import { Stack, TypeStack } from "../../shared/models/stack";
import { environment } from "../../../../environments/environment";
import { SystemService } from "../../shared/api/system.service";
import { Router } from "@angular/router";
import { NbComponentStatus } from "@nebular/theme";
import { NbAuthJWTToken, NbAuthService } from "@nebular/auth";

@Component({
  selector: "ecx-ecombox-state",
  templateUrl: "./ecombox-state.component.html",
  styleUrls: ["./ecombox-state.component.scss", "animate.css"],
})
export class EcomboxStateComponent implements OnInit, OnDestroy {
  private subscription: Subscription;

  public stacks: Stack[];
  public nbContainers = 0;
  public urlEcomBox: string;
  public urlHelp: string;
  public urlChangelog: string;
  public updateClass: string;
  public levelUpdate: NbComponentStatus;
  public currentUser = {
    username: "",
  };

  version: string = environment.ecomboxVersion;
  lastVersion: string = environment.ecomboxVersion;
  messVersion: string;

  /**
   * @param {StackStore} stackStore
   * @param {SystemService} systemService
   */
  constructor(
    private stackStore: StackStore,
    private systemService: SystemService,
    private router: Router,
    private authService: NbAuthService
  ) {
    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      if (token.isValid()) {
        this.currentUser = token.getPayload(); // here we receive a payload from the token and assigns it to our `user` variable
      }
    });
  }

  /**
   * @return {void}
   */
  ngOnInit(): void {
    this.subscription = this.stackStore.getStacks().subscribe((stacks) => {
      this.nbContainers = 0;
      this.stacks = stacks;

      // we get api's URL to display it on the dashboard but only the domain name (or IP address)
      if (this.stackStore.getUseReverseProxy() === "O") {
        this.urlEcomBox = this.stackStore.getFQDN();
      } else {
        const constructUrl = this.systemService.basePath.split("/");
        const host = constructUrl[2].split(":");
        this.urlEcomBox = host[0];
      }

      stacks.forEach((stack) => {
        if (stack.getTypeStack() === TypeStack.default) {
          this.nbContainers += 1;
        }
      });
    });

    this.systemService.searchUpdate().subscribe(
      (response) => {
        if (response) {
          this.lastVersion = response.version;
          this.messVersion = response.message;
          this.urlChangelog = response.changelog;
          switch (response.level) {
            case 1:
              this.levelUpdate = "success";
              this.updateClass = "update-minor";
              break;
            case 2:
              this.levelUpdate = "warning";
              this.updateClass = "update-inter";
              break;
            case 3:
              this.levelUpdate = "danger";
              this.updateClass = "update-major";
              break;
            default:
              this.levelUpdate = "primary";
              this.updateClass = "title-config-etat";
          }
          if (response.helpUpdate) {
            this.urlHelp = response.helpUpdate;
          }
        }
      },
      (error: Error) => {
        console.error(error.message);
      }
    );
  }

  /**
   * @return {void}
   */
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  /**
   * No Param - void return
   */
  onRefresh(): void {
    // this.getInfo();
  }

  /**
   *
   */
  public openHelpInstall(): void {
    window.open(this.urlHelp, "_blank");
  }
}
