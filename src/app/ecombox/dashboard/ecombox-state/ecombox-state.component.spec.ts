import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { EcomboxStateComponent } from './ecombox-state.component';
import {StackStore} from "../../shared/store/stack.store";
import {ConstainerService} from "../../shared/api/container.service";
import {SystemService} from "../../shared/api/system.service";
import {StacksService} from "../../shared/api/stacks.service";
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from "@angular/common/http/testing";

describe('EcomboxStateComponent', () => {
  let component: EcomboxStateComponent;
  let fixture: ComponentFixture<EcomboxStateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EcomboxStateComponent],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule
      ],
      providers: [
          StackStore,
          ConstainerService,
          SystemService,
          StacksService
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EcomboxStateComponent);
    component = fixture.debugElement.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('has the right title without any website', () => {
    component.nbContainers = 0;
    const stateElement: HTMLElement = fixture.nativeElement;
    const title = stateElement.querySelector('.title-config-etat');
    expect(title.textContent).toEqual("Aucun site créé");
  });

  it('has the right title with websites', () => {
    const websites = 5;
    component.nbContainers = websites;
    fixture.detectChanges();
    const stateElement: HTMLElement = fixture.nativeElement;
    const title = stateElement.querySelector('.title-config-etat');
    expect(title.textContent).toEqual('' + websites);
  });
});
