/* eslint-disable @typescript-eslint/no-explicit-any */
import { AfterViewInit, Component, OnDestroy } from "@angular/core";
import { NbThemeService } from "@nebular/theme";
import { Subscription } from "rxjs";
import { StackStore } from "../../shared/store/stack.store";
import { I18nPluralPipe } from "@angular/common";
import { TypeStack } from "../../shared/models/stack";

@Component({
  selector: "ecx-ecombox-server-pie",
  templateUrl: "./ecombox-server-pie.component.html",
  styleUrls: ["./ecombox-server-pie.component.scss"],
  providers: [I18nPluralPipe],
})
export class EcomboxServerPieComponent implements AfterViewInit, OnDestroy {
  nbStarted = 0;
  nbStopped = 0;
  options: any = {};
  themeSubscription: any;

  startMapping: { [k: string]: string } = {
    "=0": "aucun démarré",
    "=1": "# démarré",
    other: "# démarrés",
  };

  stopMapping: { [k: string]: string } = {
    "=0": "aucun arrêté",
    "=1": "# arrêté",
    other: "# arrêtés",
  };

  private subscription: Subscription;

  /**
   *
   * @param {NbThemeService} theme
   */
  constructor(
    private theme: NbThemeService,
    private stackStore: StackStore,
    private pluralPipe: I18nPluralPipe
  ) {}

  /**
   *
   */
  getInfo(): void {
    this.subscription = this.stackStore.getStacks().subscribe((stacks) => {
      this.nbStarted = 0;
      this.nbStopped = 0;

      stacks.forEach((stack) => {
        if (stack.getTypeStack() === TypeStack.default) {
          if (stack.getStatus() === 1) {
            this.nbStarted += 1;
          } else {
            this.nbStopped += 1;
          }
        }
      });

      this.themeSubscription = this.theme.getJsTheme().subscribe((config) => {
        const colors = config.variables;
        // tslint:disable-next-line:no-shadowed-variable
        const echarts: any = config.variables.echarts;
        const textStarted = this.pluralPipe.transform(
          this.nbStarted,
          this.startMapping
        );
        const textStopped = this.pluralPipe.transform(
          this.nbStopped,
          this.stopMapping
        );

        this.options = {
          backgroundColor: echarts.bg,
          color: [colors.successLight, colors.dangerLight],
          tooltip: {
            trigger: "item",
            formatter: "{a} <br/>{b} : ({d}%)",
          },
          legend: {
            orient: "vertical",
            left: "left",
            data: [textStarted, textStopped],
            textStyle: {
              color: echarts.textColor,
            },
          },
          series: [
            {
              name: "Sites",
              type: "pie",
              radius: "80%",
              center: ["50%", "50%"],
              data: [
                {
                  value: this.nbStarted,
                  name: textStarted,
                },
                {
                  value: this.nbStopped,
                  name: textStopped,
                },
              ],
              itemStyle: {
                emphasis: {
                  shadowBlur: 10,
                  shadowOffsetX: 0,
                  shadowColor: echarts.itemHoverShadowColor,
                },
              },
              label: {
                normal: {
                  textStyle: {
                    color: echarts.textColor,
                  },
                },
              },
              labelLine: {
                normal: {
                  lineStyle: {
                    color: echarts.axisLineColor,
                  },
                },
              },
            },
          ],
        };
      });
    });
  }

  /**
   *
   */
  ngAfterViewInit(): void {
    this.getInfo();
  }

  /**
   *
   */
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.themeSubscription.unsubscribe();
  }
}
