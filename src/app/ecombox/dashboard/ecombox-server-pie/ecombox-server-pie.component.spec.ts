import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EcomboxServerPieComponent } from './ecombox-server-pie.component';

describe('EcomboxServerPieComponent', () => {
  let component: EcomboxServerPieComponent;
  let fixture: ComponentFixture<EcomboxServerPieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EcomboxServerPieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EcomboxServerPieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
