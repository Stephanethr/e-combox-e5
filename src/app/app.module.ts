/* eslint-disable @typescript-eslint/explicit-function-return-type */
/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import {
  HttpClient,
  HttpClientModule,
  HttpResponse,
  HTTP_INTERCEPTORS,
} from "@angular/common/http";
import { CoreModule } from "./@core/core.module";
import { ThemeModule } from "./@theme/theme.module";
import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";
import { TokenInterceptor } from "./ecombox/shared/api/token-interceptor";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { NbMenuModule, NbSidebarModule } from "@nebular/theme";
import { ToastrModule } from "ngx-toastr";
// import { AuthService } from "./ecombox/shared/api/auth.service";
import { AuthGuard } from "./auth-guard.service";
import { NbEvaIconsModule } from "@nebular/eva-icons";
import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import { API_URL } from "./ecombox/shared/api/variables";

import {
  NbPasswordAuthStrategy,
  NbAuthModule,
  getDeepFromObject,
  NbPasswordAuthStrategyOptions,
  NbAuthJWTToken,
} from "@nebular/auth";
import { NbRoleProvider, NbSecurityModule } from "@nebular/security";
import { RoleProvider } from "./ecombox/role.provider";

/**
 *
 * @param {HttpClient} http
 * @return {TranslateHttpLoader}
 */
export function HttpLoaderFactory(http: HttpClient): TranslateLoader {
  return new TranslateHttpLoader(http, "./assets/i18n/", ".json");
}

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    NbEvaIconsModule,
    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    CoreModule.forRoot(),
    ThemeModule.forRoot(),
    ToastrModule.forRoot(),
    TranslateModule.forRoot({
      defaultLanguage: "fr",
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    NbAuthModule.forRoot({
      strategies: [
        NbPasswordAuthStrategy.setup({
          name: "username",
          baseEndpoint: API_URL,
          login: {
            endpoint: "/auth",
            method: "post",
            redirect: {
              success: "/ecombox/dashboard",
              failure: "/auth/login", // stay on the same page
            },
            defaultErrors: [
              "L'identifiant ou le mot de passe est incorrect. Veuillez retenter l'opération.",
            ],
            defaultMessages: ["Vous avez été identifié avec succès"],
          },
          logout: {
            endpoint: "/auth/logout",
            method: "post",
            redirect: {
              success: "/auth/login",
            },
          },
          token: {
            class: NbAuthJWTToken,
            getter: (
              module: string,
              res: HttpResponse<Object>,
              options: NbPasswordAuthStrategyOptions
            ) => getDeepFromObject(res.body, "jwt"),
          },
        }),
      ],
      forms: {
        login: {
          strategy: "username",
        },
      },
    }),
    // ACL for role provider
    NbSecurityModule.forRoot({
      accessControl: {
        2: {
          view: [
            "dashboard",
            "presta",
            "woocommerce",
            "blog",
            "mautic",
            "suitecrm",
            "odoo",
            "kanboard",
            "humhub",
            "admin",
            "sftp",
            "pma",
            "images",
            "help",
            "policies",
          ],
        },
        1: {
          create: "policies",
          view: "*",
        },
      },
    }),
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    AuthGuard,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
    { provide: NbRoleProvider, useClass: RoleProvider },
  ],
})
export class AppModule {}
