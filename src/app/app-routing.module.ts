import { RouterModule, Routes, ExtraOptions } from "@angular/router";
import { NgModule } from "@angular/core";
import { AuthGuard } from "./auth-guard.service";

const config: ExtraOptions = {
  useHash: false,
};

const routes: Routes = [
  {
    path: "auth",
    loadChildren: "./auth/auth.module#AuthModule",
  },
  {
    path: "ecombox",
    canActivate: [AuthGuard],
    loadChildren: "./ecombox/ecombox.module#EcomboxModule",
  },
  { path: "", redirectTo: "auth/login", pathMatch: "full" },
  { path: "**", redirectTo: "auth/login" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
