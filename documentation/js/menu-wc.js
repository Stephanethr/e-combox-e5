'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">e-combox documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                        <li class="link">
                            <a href="changelog.html"  data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>CHANGELOG
                            </a>
                        </li>
                        <li class="link">
                            <a href="contributing.html"  data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>CONTRIBUTING
                            </a>
                        </li>
                        <li class="link">
                            <a href="license.html"  data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>LICENSE
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-52b3baf6a8c75531a471c0e762e14aaf61e45a387413d9a30c67319aabe8cacf2583ffbd7e43773a35243c489231b7de707f7cffa83a242814d5b167bc6a47ce"' : 'data-target="#xs-components-links-module-AppModule-52b3baf6a8c75531a471c0e762e14aaf61e45a387413d9a30c67319aabe8cacf2583ffbd7e43773a35243c489231b7de707f7cffa83a242814d5b167bc6a47ce"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-52b3baf6a8c75531a471c0e762e14aaf61e45a387413d9a30c67319aabe8cacf2583ffbd7e43773a35243c489231b7de707f7cffa83a242814d5b167bc6a47ce"' :
                                            'id="xs-components-links-module-AppModule-52b3baf6a8c75531a471c0e762e14aaf61e45a387413d9a30c67319aabe8cacf2583ffbd7e43773a35243c489231b7de707f7cffa83a242814d5b167bc6a47ce"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AppModule-52b3baf6a8c75531a471c0e762e14aaf61e45a387413d9a30c67319aabe8cacf2583ffbd7e43773a35243c489231b7de707f7cffa83a242814d5b167bc6a47ce"' : 'data-target="#xs-injectables-links-module-AppModule-52b3baf6a8c75531a471c0e762e14aaf61e45a387413d9a30c67319aabe8cacf2583ffbd7e43773a35243c489231b7de707f7cffa83a242814d5b167bc6a47ce"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AppModule-52b3baf6a8c75531a471c0e762e14aaf61e45a387413d9a30c67319aabe8cacf2583ffbd7e43773a35243c489231b7de707f7cffa83a242814d5b167bc6a47ce"' :
                                        'id="xs-injectables-links-module-AppModule-52b3baf6a8c75531a471c0e762e14aaf61e45a387413d9a30c67319aabe8cacf2583ffbd7e43773a35243c489231b7de707f7cffa83a242814d5b167bc6a47ce"' }>
                                        <li class="link">
                                            <a href="injectables/AuthService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AuthService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link" >AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/DashboardModule.html" data-type="entity-link" >DashboardModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-DashboardModule-db2a004d26ed3a81ba406d663a31bf97e7622b51472afee4d51a12deeb45bec80523bab6aca70f0f30267227db37d8a8f93a6db93ca5eaafcff6938dd344bf4d"' : 'data-target="#xs-components-links-module-DashboardModule-db2a004d26ed3a81ba406d663a31bf97e7622b51472afee4d51a12deeb45bec80523bab6aca70f0f30267227db37d8a8f93a6db93ca5eaafcff6938dd344bf4d"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-DashboardModule-db2a004d26ed3a81ba406d663a31bf97e7622b51472afee4d51a12deeb45bec80523bab6aca70f0f30267227db37d8a8f93a6db93ca5eaafcff6938dd344bf4d"' :
                                            'id="xs-components-links-module-DashboardModule-db2a004d26ed3a81ba406d663a31bf97e7622b51472afee4d51a12deeb45bec80523bab6aca70f0f30267227db37d8a8f93a6db93ca5eaafcff6938dd344bf4d"' }>
                                            <li class="link">
                                                <a href="components/DashboardComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >DashboardComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/EcomboxServerDetailsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >EcomboxServerDetailsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/EcomboxServerPieComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >EcomboxServerPieComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/EcomboxStateComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >EcomboxStateComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/EcomboxUsageStatsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >EcomboxUsageStatsComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/EcomboxModule.html" data-type="entity-link" >EcomboxModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-EcomboxModule-bf0f5fafac633cfdfed061ced850d412f4668971cfd51fb298119546013a2d9fbd7a87a01dd63c943ac88a1ee488e471a763fa84afc8f640ffdbd2dfa0e1269a"' : 'data-target="#xs-components-links-module-EcomboxModule-bf0f5fafac633cfdfed061ced850d412f4668971cfd51fb298119546013a2d9fbd7a87a01dd63c943ac88a1ee488e471a763fa84afc8f640ffdbd2dfa0e1269a"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-EcomboxModule-bf0f5fafac633cfdfed061ced850d412f4668971cfd51fb298119546013a2d9fbd7a87a01dd63c943ac88a1ee488e471a763fa84afc8f640ffdbd2dfa0e1269a"' :
                                            'id="xs-components-links-module-EcomboxModule-bf0f5fafac633cfdfed061ced850d412f4668971cfd51fb298119546013a2d9fbd7a87a01dd63c943ac88a1ee488e471a763fa84afc8f640ffdbd2dfa0e1269a"' }>
                                            <li class="link">
                                                <a href="components/EcomboxComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >EcomboxComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/HelpComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >HelpComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/EcomboxRoutingModule.html" data-type="entity-link" >EcomboxRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ImagesModule.html" data-type="entity-link" >ImagesModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ImagesModule-4731bccccf1fe0d351b87cff9b2719ab61d59f0840ec51eb81691d68a1cb15b91804e5a7930c9171ae6bc4b9c0dcbed52b9f30064ddec15089f3bb23bbc310a4"' : 'data-target="#xs-components-links-module-ImagesModule-4731bccccf1fe0d351b87cff9b2719ab61d59f0840ec51eb81691d68a1cb15b91804e5a7930c9171ae6bc4b9c0dcbed52b9f30064ddec15089f3bb23bbc310a4"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ImagesModule-4731bccccf1fe0d351b87cff9b2719ab61d59f0840ec51eb81691d68a1cb15b91804e5a7930c9171ae6bc4b9c0dcbed52b9f30064ddec15089f3bb23bbc310a4"' :
                                            'id="xs-components-links-module-ImagesModule-4731bccccf1fe0d351b87cff9b2719ab61d59f0840ec51eb81691d68a1cb15b91804e5a7930c9171ae6bc4b9c0dcbed52b9f30064ddec15089f3bb23bbc310a4"' }>
                                            <li class="link">
                                                <a href="components/ImagesComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ImagesComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/MiscellaneousModule.html" data-type="entity-link" >MiscellaneousModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-MiscellaneousModule-63b4679166c81d0acb69ff8b6b1324ee227ede20c31745981f28f9ac3c1752ffc712a178e70447fe7ef27838c6025f8be7d486c92376ee0e3a4f4e87f0a14df3"' : 'data-target="#xs-components-links-module-MiscellaneousModule-63b4679166c81d0acb69ff8b6b1324ee227ede20c31745981f28f9ac3c1752ffc712a178e70447fe7ef27838c6025f8be7d486c92376ee0e3a4f4e87f0a14df3"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-MiscellaneousModule-63b4679166c81d0acb69ff8b6b1324ee227ede20c31745981f28f9ac3c1752ffc712a178e70447fe7ef27838c6025f8be7d486c92376ee0e3a4f4e87f0a14df3"' :
                                            'id="xs-components-links-module-MiscellaneousModule-63b4679166c81d0acb69ff8b6b1324ee227ede20c31745981f28f9ac3c1752ffc712a178e70447fe7ef27838c6025f8be7d486c92376ee0e3a4f4e87f0a14df3"' }>
                                            <li class="link">
                                                <a href="components/MiscellaneousComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MiscellaneousComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NotFoundComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >NotFoundComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/MiscellaneousRoutingModule.html" data-type="entity-link" >MiscellaneousRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ServersModule.html" data-type="entity-link" >ServersModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ServersModule-e04300bcc844740db9e7995f6923b263bdbfaf09cfede105a63695e19bd0a056345fdb7825c7e1a699e399aa43c51f9bcf6d8b3f38437aae1a265c3a97beae7f"' : 'data-target="#xs-components-links-module-ServersModule-e04300bcc844740db9e7995f6923b263bdbfaf09cfede105a63695e19bd0a056345fdb7825c7e1a699e399aa43c51f9bcf6d8b3f38437aae1a265c3a97beae7f"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ServersModule-e04300bcc844740db9e7995f6923b263bdbfaf09cfede105a63695e19bd0a056345fdb7825c7e1a699e399aa43c51f9bcf6d8b3f38437aae1a265c3a97beae7f"' :
                                            'id="xs-components-links-module-ServersModule-e04300bcc844740db9e7995f6923b263bdbfaf09cfede105a63695e19bd0a056345fdb7825c7e1a699e399aa43c51f9bcf6d8b3f38437aae1a265c3a97beae7f"' }>
                                            <li class="link">
                                                <a href="components/DialogMultipleStacksPromptComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >DialogMultipleStacksPromptComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/DialogNamePromptComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >DialogNamePromptComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/DialogTemplateComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >DialogTemplateComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ServerAdvancedCardComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ServerAdvancedCardComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ServerAdvancedComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ServerAdvancedComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ServerBaseComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ServerBaseComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ServerStatusCardComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ServerStatusCardComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ServersCreateComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ServersCreateComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ServersManageComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ServersManageComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ShowcaseDialogComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ShowcaseDialogComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ServersRoutingModule.html" data-type="entity-link" >ServersRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/SharedModule.html" data-type="entity-link" >SharedModule</a>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-SharedModule-e4ab37a53ccbad26759ca66218180ae4ab34cbea79279dc147e40a26c436e10cc3e61e3282e22240cdf15cb9874081399042d3cb61a0407ff6348dd9404b4928"' : 'data-target="#xs-injectables-links-module-SharedModule-e4ab37a53ccbad26759ca66218180ae4ab34cbea79279dc147e40a26c436e10cc3e61e3282e22240cdf15cb9874081399042d3cb61a0407ff6348dd9404b4928"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-SharedModule-e4ab37a53ccbad26759ca66218180ae4ab34cbea79279dc147e40a26c436e10cc3e61e3282e22240cdf15cb9874081399042d3cb61a0407ff6348dd9404b4928"' :
                                        'id="xs-injectables-links-module-SharedModule-e4ab37a53ccbad26759ca66218180ae4ab34cbea79279dc147e40a26c436e10cc3e61e3282e22240cdf15cb9874081399042d3cb61a0407ff6348dd9404b4928"' }>
                                        <li class="link">
                                            <a href="injectables/ContainerService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ContainerService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/DockerRegistryService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >DockerRegistryService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/ImageService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ImageService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/StackStore.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >StackStore</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/StacksService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >StacksService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/SystemService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >SystemService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/VolumeService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >VolumeService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/Configuration.html" data-type="entity-link" >Configuration</a>
                            </li>
                            <li class="link">
                                <a href="classes/Container.html" data-type="entity-link" >Container</a>
                            </li>
                            <li class="link">
                                <a href="classes/CustomHttpUrlEncodingCodec.html" data-type="entity-link" >CustomHttpUrlEncodingCodec</a>
                            </li>
                            <li class="link">
                                <a href="classes/Image.html" data-type="entity-link" >Image</a>
                            </li>
                            <li class="link">
                                <a href="classes/Stack.html" data-type="entity-link" >Stack</a>
                            </li>
                            <li class="link">
                                <a href="classes/User.html" data-type="entity-link" >User</a>
                            </li>
                            <li class="link">
                                <a href="classes/Utils.html" data-type="entity-link" >Utils</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/ApiService.html" data-type="entity-link" >ApiService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AuthService.html" data-type="entity-link" >AuthService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ContainerAdapter.html" data-type="entity-link" >ContainerAdapter</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ContainerService.html" data-type="entity-link" >ContainerService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/DockerRegistryService.html" data-type="entity-link" >DockerRegistryService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ImageAdapter.html" data-type="entity-link" >ImageAdapter</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ImageService.html" data-type="entity-link" >ImageService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/StackAdapter.html" data-type="entity-link" >StackAdapter</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/StacksService.html" data-type="entity-link" >StacksService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/StackStore.html" data-type="entity-link" >StackStore</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/SystemService.html" data-type="entity-link" >SystemService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/VolumeService.html" data-type="entity-link" >VolumeService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interceptors-links"' :
                            'data-target="#xs-interceptors-links"' }>
                            <span class="icon ion-ios-swap"></span>
                            <span>Interceptors</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="interceptors-links"' : 'id="xs-interceptors-links"' }>
                            <li class="link">
                                <a href="interceptors/TokenInterceptor.html" data-type="entity-link" >TokenInterceptor</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#guards-links"' :
                            'data-target="#xs-guards-links"' }>
                            <span class="icon ion-ios-lock"></span>
                            <span>Guards</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="guards-links"' : 'id="xs-guards-links"' }>
                            <li class="link">
                                <a href="guards/RedirectGuard.html" data-type="entity-link" >RedirectGuard</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/Adapter.html" data-type="entity-link" >Adapter</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/AuthenticateUserResponse.html" data-type="entity-link" >AuthenticateUserResponse</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CardSettings.html" data-type="entity-link" >CardSettings</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CardSettings-1.html" data-type="entity-link" >CardSettings</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ConfigurationParameters.html" data-type="entity-link" >ConfigurationParameters</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ControlsMap.html" data-type="entity-link" >ControlsMap</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/DataUsage.html" data-type="entity-link" >DataUsage</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ExecRequest.html" data-type="entity-link" >ExecRequest</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/StackCreateRequest.html" data-type="entity-link" >StackCreateRequest</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/StackEnv.html" data-type="entity-link" >StackEnv</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/StackFileResponse.html" data-type="entity-link" >StackFileResponse</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/StackMigratePayload.html" data-type="entity-link" >StackMigratePayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/StackUpdateRequest.html" data-type="entity-link" >StackUpdateRequest</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/enumerations.html" data-type="entity-link">Enums</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/typealiases.html" data-type="entity-link">Type aliases</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});