# V4
C'est la version qui va permettre de déployer l'e-comBox à grande échelle dans les régions. 
Elle permettra notamment :
- l'authentification des professeurs à l'interface sur l'annuaire centralisateur
- le passage par un reverse proxy


## 4.0.0
* Module permettant l'authentification des professeurs sur l'interface via l'annuaire centralisateur (avec éventuellement une intégration au GAR).


# V3

## Pour toutes les versions

* **kanboard** : mise à jour vers la version 1.2.21, intégration des plugins et possibilité de les installer via l'interface. 

## 3.2.0

### Fonctionnalités

* **Partager ses propres modèles de sites** : le partage de modèles de sites sera possible directement à partir de l'interface e-comBox après avoir créé un compte sur la plateforme Docker Hub
* **Récupérer un modèle de site créé par un utilisateur de la communauté** : il sera désormais possible, à partir du nom complet du modèle, de créer un site à partir d'un modèle partagé par un utilisateur de la communauté e-comBox.


## 3.1.0

- La **v3.1** est prévue pour le **28/02/2022**.


**Lien vers les "issues"** : https://gitlab.com/groups/e-combox/-/issues


### Sécurisation

* Intégration du protocole HTTPS au niveau des sites (**V3.1**).


### Bug

* Fix version in the footer


### Sites

* **Tous les types de site** : mise à jour des versions.
* **Général** : sauvegarde/restauration d'un seul site (en demandant un chemin à l’utilisateur).
* **Prestashop** : intégration d'un module tchat sur Prestashop.


### Fonctionnalités

* **Gérer vos modèles de site** : possibilité de renommer les images personnalisées par les profs (**V3.1**).
* **Sauvegarde et restauration d'un seul site.
* **En bas du menu de gauche** (sous Aide) : Intégration de CGU personnalisables (**V3.1**).
* **Mise à jour sur Linux** : automatiser les mises à jour (aucune action nécessaire pour l'administrateur sauf à lancer la mise à jour)

### Divers

* Rendre l'application un peu plus "responsive".
* Automatiser l'ajout des images personnalisées.
* Automatiser le partage d'un modèle de site créé avec l'e-comBox.
