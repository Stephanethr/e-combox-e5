# e-comBox_webapp

## Objectif de l'application

L’application e-comBox permet d'installer le plus simplement possible, au sein d'un réseau d'établissement, sur un portable personnel ou chez un hébergeur en ligne (serveur OVH par exemple), plusieurs instances de différentes applications (prestashop, wordpress, mautic, odoo, kanboard, etc) sous forme de conteneurs docker : les serveurs pourront être créés à la demande en tant que de besoin. L’idée est ici de répondre aux besoins pédagogiques les plus étendus des enseignants tout en réduisant les contraintes techniques au minimum.

L’application e-comBox est elle-même une application _web_ “dockérisée” installable sur n’importe quel système d’exploitation (Windows 10, Linux et MacOS) qui permet de lancer et gérer le ou les conteneurs dans lesquels le service est installé.

Pour en savoir plus : https://llb.ac-corse.fr/mw/


## Documentation

Pour accéder à la documentation :  
> https://e-combox.gitlab.io/e-combox_webapp/

La documentation est mis à jour automatiquement à chaque push